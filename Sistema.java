import java.util.*;
import java.io.*;
import java.text.*;

public class Sistema{

    private static BaseDados basedados = new BaseDados();
    private static Rede rede = new Rede();
    private static Menu menuLogin,menuUsuario,menuAdmin,menuPedidoAmizade,menuDecPedidoAmizade,menuListaAmigos,menuCriarCache,menuProcurarCache,menuDecProcura,menuDecCache,menuRegCache,menuEditar,menuDecAtividades,menuEstat;
    private static int id=0;

    public static void main(String[] args){
        try{
            carregaDados();
            carregaDados2();
        }catch(IOException e){System.out.println("IO exception");}
        catch(ClassNotFoundException e){System.out.println("class exception");}
        criaAdmins();
        carregaMenus();
        menuInicial();
    }

    public static void criaAdmins(){
        String admin1="PAULO@admin";
        String admin2="DIOGO@admin";
        if(!(rede.getRede().containsKey(admin1) && rede.getRede().containsKey(admin2))) {
            GregorianCalendar dnadm1=new GregorianCalendar(1993,04,11);
            GregorianCalendar dnadm2=new GregorianCalendar(1994,05,20);
            Usuario adminpaulo=new Usuario(admin1,"adminpaulo","Paulo","Masculino","Brasil","Goias","Anapolis",dnadm1);
            Usuario admindiogo=new Usuario(admin2,"admindiogo","Diogo","Masculino","Portugal","Braga","Braga",dnadm2);
            try{
                rede.addUsuario(adminpaulo);
                rede.addUsuario(admindiogo);
            }
            catch (UsuarioExisteException e){
                System.out.println("Admin PAULO e DIOGO criados");
            }
        }

    }

    public static void menuInicial(){
        do {
            clean();
            System.out.println("************ GeocachingPOO ************");
            menuLogin.executa();
            switch (menuLogin.getOpcao()){
                case 1:login();

                case 2:criaUsuario();
            }
        }while (menuLogin.getOpcao()!=0);
        sair();
    }

    public static void login(){
        int aux=0;
        System.out.println("Insira o email");
        String email=Input.lerString();
        System.out.println("Insira a password");
        String pass=Input.lerString();
        try{
            aux=rede.login(email,pass);
        }
        catch(UsuarioExisteException e){
            System.out.println("Usuario inexistente");
            mostraMensagem();
            menuInicial();
        }
        if(aux==0){
            System.out.println("Password Inv�lida");
            mostraMensagem();
            menuInicial();
        }
        if(email.contains("@admin")) execMenuAdmin(email);
        execMenuUsuario(email);
    }

    public static void criaUsuario(){
        System.out.println("Insira o email");
        String email=Input.lerString();
        System.out.println("Insira a password");
        String pass=Input.lerString();
        System.out.println("Insira o nome");
        String nome=Input.lerString();
        System.out.println("Insira o genero");
        String genero=Input.lerString();
        System.out.println("Insira o pais");
        String pais=Input.lerString();
        System.out.println("Insira a regiao");
        String regiao=Input.lerString();
        System.out.println("Insira a cidade");
        String cidade=Input.lerString();
        System.out.println("Introduza a sua data de nascimento no formato dia,mes,ano separado por 'Enters'");
        int dia;
        int mes;
        int ano;
        while(true){
            System.out.println("Tem que ter pelo menos 5 anos para se registar ");
            System.out.println("Valores pemitidos -  dia [1,31],  mes[1,12], ano [1915,2010] ");
            dia=Input.lerInt();
            mes=Input.lerInt();
            ano=Input.lerInt();
            if(ano>=1915 && ano <=2010 && dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
        }
        GregorianCalendar dn=new GregorianCalendar(ano,(mes-1),dia);
        Usuario u=new Usuario(email,pass,nome,genero,pais,regiao,cidade,dn);
        try{
            rede.addUsuario(u);
        }
        catch(UsuarioExisteException e){
            System.out.println("Usuario j� existe");
            mostraMensagem();
        }
        System.out.println("Usuario criado com sucesso");
        mostraMensagem();
        menuInicial();
    }

    public static void execMenuUsuario(String u){
        do{
            clean();
            menuUsuario.executa();  
            switch (menuUsuario.getOpcao()){
                case 1: verListaAmigos(u);
                break;
                case 2: execMenuPedidoAmizade(u);
                break;
                case 3: execMenuCriarCache(u);
                break;
                case 4: verCachesCriadas(u);
                break;
                case 5: verAtividades(u);
                break;
                case 6: execMenuProcurarCache(u);
                break;
                case 7: execMenuEditar(u);
                break;
                case 8: visitar(u);
                break;
                case 9: execMenuVerEstatisticas(u);
                break;
            }
        }while(menuUsuario.getOpcao()!=0);
        clean();
        menuInicial(); 
    }

    public static void execMenuAdmin(String u){
    do{
    clean();
    menuAdmin.executa();  
    switch (menuAdmin.getOpcao()){
    case 1: simularEvento();
    break;

    }
    }while(menuAdmin.getOpcao()!=0);
    clean();
    menuInicial(); 
    }

    public static void verListaAmigos(String u){
        TreeSet<Usuario> aux = rede.getUser(u).getListaAmigos();
        int flag=1;
        clean();
        if(aux.size()==0){
            System.out.println("A sua lista de amigos encontra-se vazia.");
            mostraMensagem();
            execMenuUsuario(u);
        }else{
            for(Usuario a:aux){
                if(flag==0){
                    execMenuUsuario(u);
                }else{
                    flag=execMenuAmigo(u,a.getEmail());   
                }
            }
            if(flag==0){execMenuUsuario(u);}
            else{
                System.out.println("N�o tens mais amigos");
                mostraMensagem();
                execMenuUsuario(u);
            }
        }
    }

    public static int execMenuAmigo(String u, String s){ //u -> mail para onde vamos voltar s-> quem estamos a visitar
        clean();
        int op;
        do{
            System.out.println(rede.getUser(s).toString());
            menuListaAmigos.executa();
            switch (op=menuListaAmigos.getOpcao()){
                case 1: verCachesAmigo(u,s);
                break;
                case 2: verAtividadesAmigo(u,s);
                break;
                case 3: remAmigo(u,s);
                break;
                case 4: clean();
                break;
            }
        }while (menuListaAmigos.getOpcao() !=0 && menuListaAmigos.getOpcao() !=3 && menuListaAmigos.getOpcao() !=4);
        clean();
        return op;
    }

    public static void verCachesAmigo(String u,String s){
        clean();
        ArrayList<Cache> aux = rede.getUser(s).getListaCaches();
        if(aux.size()==0){
            System.out.println("O teu amigo n�o tem caches criadas.");
            mostraMensagem();
            execMenuAmigo(u,s);
        }else{
            for(Cache a:aux){
                System.out.println(a.toString());
            }
        }
        mostraMensagem();
        execMenuAmigo(u,s);
    }

    public static void remAmigo(String u,String s){
        Usuario aux = new Usuario (rede.getUser(u));
        Usuario aux2 = new Usuario (rede.getUser(s));
        aux.remAmigo(aux2);
        aux2.remAmigo(aux);
        rede.replaceUsuario(aux);
        rede.replaceUsuario(aux2);
        System.out.println("Amigo removido com sucesso");
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void verAtividadesAmigo(String u,String s){
        TreeSet<Actividade> listaAux =rede.getUser(s).getListaActividades();
        Iterator<Actividade> it=listaAux.descendingIterator();
        if(listaAux.isEmpty()){
            System.out.println("Nao tem nenhuma actividade registada");
            mostraMensagem();
            execMenuUsuario(u);
        }else{
            int cont=1;
            while(it.hasNext() && cont<11){
                SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy '�s' HH:mm:ss");
                Actividade a=it.next();
                String auxS=aux.format(a.getDataReg().getTime());
                System.out.println(cont);
                System.out.println("Dono da cache: "+a.getDonoCache());
                System.out.println("Tipo de cache: "+a.getTipoCache());
                System.out.println(a.getLatLong());
                System.out.println(auxS);
                cont++;

            }
            execMenuAtividades(u);
        }
    }

    public static void execMenuPedidoAmizade(String u){
        do {
            clean();
            menuPedidoAmizade.executa();
            switch (menuPedidoAmizade.getOpcao()) {
                case 1: verPedidos(u);
                break;
                case 2: mandarPedido(u);
                break;
            }
        }while (menuPedidoAmizade.getOpcao()!=0);
        execMenuUsuario(u);  
    }

    public static void verPedidos(String u){
        clean();
        ArrayList<Usuario> aux = rede.getUser(u).getListaPedidos();
        int i =0;
        if(aux.size()==0){
            System.out.println("Nao tem pedidos");
            mostraMensagem();
            execMenuPedidoAmizade(u);
        }else{
            for(Usuario a:aux){
                System.out.println(a.toString());
                i=execMenuDecPedidoAmizade(u,a,i);
                i++;
            }
        }
        execMenuPedidoAmizade(u);
    }

    public static void mandarPedido(String u){
        System.out.println("Insira o email de quem quer pedir em amizade");
        String a = Input.lerString();
        try{rede.enviaPedido(u,a);}
        catch(UsuarioExisteException e){
            System.out.println("Usuario n�o existe");
            mostraMensagem();
            execMenuPedidoAmizade(u);
        }
        catch (AmigoExisteException e){
            System.out.println("O usuario j� existe na sua lista de amigos");
            mostraMensagem();
            execMenuPedidoAmizade(u);
        }
        System.out.println("Pedido de amizade enviado com sucesso");
        mostraMensagem();
        execMenuPedidoAmizade(u);
    }

    public static int execMenuDecPedidoAmizade(String u,Usuario a,int x){
        do {
            menuDecPedidoAmizade.executa(); 
            switch (menuDecPedidoAmizade.getOpcao()) {
                case 1: x=aceitarPedido(u,a,x);
                break;

                case 2: x=rejeitarPedido(u,x);
                break;
            }
        }while (menuDecPedidoAmizade.getOpcao()!=1 && menuDecPedidoAmizade.getOpcao()!=2 && menuDecPedidoAmizade.getOpcao()!=0);
        return x;
    }

    public static int aceitarPedido (String u,Usuario a,int x){
        //STRING U - EMAIL DE QUEM RECEBE O PEDIDO
        //USUARIOR A - USUARIO QUE ENVIA O PEDIDO
        // add amigo nos dois e remover pedido
        // x ->posi��o
        Usuario aux = new Usuario (rede.getUser(u));
        aux.addAmigo(a);
        a.addAmigo(aux);
        int i=aux.removePedido(x);
        rede.replaceUsuario(aux);
        rede.replaceUsuario(a);
        return i;
    }

    public static int rejeitarPedido (String u,int x){
        int i;
        Usuario aux=new Usuario(rede.getUser(u));
        i=aux.removePedido(x);
        rede.replaceUsuario(aux);
        return i;
    }

    public static void verCachesCriadas(String u){
        clean();
        int flag=1;
        ArrayList<Cache> aux = rede.getUser(u).getListaCaches();
        if(aux.size()==0){
            System.out.println("Nao tens caches criadas");
            mostraMensagem();
            execMenuUsuario(u);
        }else{
            for(Cache c:aux){
                if(flag==0){
                    execMenuUsuario(u);
                }else{
                    flag=execMenuDecCache(u,c);
                }
            }
            if(flag==0){execMenuUsuario(u);}
            else{
                System.out.println("N�o tens mais caches criadas");
                mostraMensagem();
                execMenuUsuario(u);
            }
        }
    }

    public static int execMenuDecCache(String u,Cache c){
        int op;
        do {
            System.out.println(c.toString());
            menuDecCache.executa(); 
            switch (op=menuDecCache.getOpcao()) {
                case 1: apagarCache(u,c);
                break;

                case 2: clean();
                break;
            }
        }while (menuDecCache.getOpcao()!=1 && menuDecCache.getOpcao()!=2 && menuDecCache.getOpcao()!=0);
        return op;
    }

    public static void apagarCache(String u,Cache c){
        Usuario aux = new Usuario (rede.getUser(u));
        aux.remCache(c);
        basedados.remCacheBD(aux,c);
        rede.replaceUsuario(aux);
        System.out.println("Cache removida com sucesso");
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void execMenuVerEstatisticas(String u){
        do{
            menuEstat.executa();
            switch(menuEstat.getOpcao()){
                case 1: verEstatAnual(u);
                break;
                case 2: verEstatMensal(u);
                break;
            }
        }while(menuEstat.getOpcao()!=0);
    }

    public static void verEstatAnual(String u){
        System.out.println("Insira um ano");
        int ano = Input.lerInt();
        int i=1;
        HashMap<Integer,ArrayList<Estatistica>> aux = rede.getUser(u).getEstatisticas();
        ArrayList<Estatistica> estat=aux.get(ano);
        if(aux.containsKey(ano)){
            if(!estat.isEmpty()){
                for(Estatistica e : estat){
                    System.out.println("Estatisticas do mes n�mero : "+i);
                    System.out.println(e.toString());
                    i++;
                }
                mostraMensagem();
                execMenuUsuario(u);
            }else System.out.println("N�o existe estatisticas nesse ano");
        }else System.out.println("N�o existe estatisticas nesse ano");
    }

    public static void verEstatMensal(String u){
        System.out.println("Insira um ano e um mes separado por 'enters'");
        int ano = Input.lerInt();
        int mes = Input.lerInt();
        HashMap<Integer,ArrayList<Estatistica>> aux = rede.getUser(u).getEstatisticas();
        ArrayList<Estatistica> estat=aux.get(ano);
        if(aux.containsKey(ano)){
            if(!estat.isEmpty()){
                if(estat.get(mes)!=null){
                    System.out.println("Estatisticas do ano : "+ano+" do mes : "+mes);
                    System.out.println(estat.get(mes-1).toString());
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("N�o existe estatisticas nesse m�s");
            }else System.out.println("N�o existe estatisticas nesse ano");
        }else System.out.println("N�o existe estatisticas nesse ano");
    }

    public static void verAtividades(String u){
        TreeSet<Actividade> listaAux =rede.getUser(u).getListaActividades();
        Iterator<Actividade> it=listaAux.descendingIterator();
        if(listaAux.isEmpty()){
            System.out.println("Nao tem nenhuma actividade registada");
            mostraMensagem();
            execMenuUsuario(u);
        }else{
            int cont=1;
            while(it.hasNext() && cont<11){
                SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy '�s' HH:mm:ss");
                Actividade a=it.next();
                String auxS=aux.format(a.getDataReg().getTime());
                System.out.println(cont);
                System.out.println("Dono da cache: "+a.getDonoCache());
                System.out.println("Tipo de cache: "+a.getTipoCache());
                System.out.println(a.getLatLong());
                System.out.println(auxS);
                cont++;

            }
            execMenuAtividades(u);
        }
    }

    public static void execMenuAtividades(String u){
        do {
            menuDecAtividades.executa(); 
            switch (menuDecAtividades.getOpcao()) {
                case 1: verDetalhes(u);
                break;
            }
        }while (menuDecAtividades.getOpcao()!=1 && menuDecAtividades.getOpcao()!=0);

    }

    public static void verDetalhes(String u){
        TreeSet<Actividade> aux = rede.getUser(u).getListaActividades();
        int i=1;
        Iterator<Actividade> it=aux.descendingIterator();
        System.out.println("Insira o n� da atividade que deseja ver os detalhes");
        int x=Input.lerInt();
        if(x>10 || x<1){ System.out.println("So pode escolher uma das actividades da lista"); verAtividades(u);}
        while(it.hasNext()){
            Actividade a=it.next();
            if(i==x){
                System.out.println(a.toString());
                mostraMensagem();
                break;

            }
            i++;
        }
        execMenuAtividades(u);
    }

    public static void execMenuCriarCache(String u){
        do {
            clean();
            menuCriarCache.executa();
            switch (menuCriarCache.getOpcao()) {
                case 1: criarMicro(u);
                break;
                case 2: criarTradicional(u);
                break;
                case 3: criarMisterio(u);
                break;
                case 4: criarEvento(u);
                break; 
                case 5: criarVirtual(u);
                break;
                case 6: criarMulti(u);
                break;
            }
        }while (menuPedidoAmizade.getOpcao()!=0);
        execMenuUsuario(u);  
    }

    public static void execMenuProcurarCache(String u){
        do {
            clean();
            menuProcurarCache.executa();
            switch (menuProcurarCache.getOpcao()) {
                case 1: procuraCacheUsuario(u);
                break;
                case 2: procurarCachePais(u);
                break;
                case 3: procuraCacheRegiao(u);
                break;
            }
        }while (menuPedidoAmizade.getOpcao()!=0);
        execMenuUsuario(u);  
    }

    public static void procuraCacheUsuario(String u){
        clean();
        int flag=1;
        System.out.println("Insira o mail do usuario, para procurar as suas caches:");
        String usuario = Input.lerString();
        ArrayList<Cache> aux;
        aux = basedados.getBaseUsuario().get(usuario);
        if(!basedados.getBaseUsuario().containsKey(usuario)){
            System.out.println("Esse usuario n�o tem caches criadas");
            mostraMensagem();
            execMenuProcurarCache(u);
        }else{
            for(Cache c:aux){
                if(flag==0){
                    execMenuProcurarCache(u);
                }else{
                    flag=execMenuDecProcuraU(u,c);   
                }
            }
            if(flag==0){execMenuProcurarCache(u);}
            else{
                System.out.println("Este usuario n�o tem mais caches");
                mostraMensagem();
                execMenuProcurarCache(u);
            }
        }
    }

    public static void procurarCachePais(String u){
        clean();
        int flag=1;
        System.out.println("Insira o pa�s onde quer procurar caches");
        String pais= Input.lerString();
        ArrayList<Cache> aux=basedados.getBasePais().get(pais);
        if(!basedados.getBasePais().containsKey(pais)){ 
            System.out.println("Esse pais n�o tem caches criadas");
            mostraMensagem();
            execMenuProcurarCache(u);
        }  
        else{
            for(Cache c:aux){
                if(flag==0){
                    execMenuProcurarCache(u);
                }else{
                    flag=execMenuDecProcuraP(u,c);   
                }
            }
            if(flag==0){execMenuProcurarCache(u);}
            else{
                System.out.println("N�o existem mais caches neste pais");
                mostraMensagem();
                execMenuProcurarCache(u);
            }
        }

    }

    public static void procuraCacheRegiao(String u){
        clean();
        int flag=1;
        System.out.println("Insira a regi�o onde quer procurar caches");
        String regiao = Input.lerString();
        ArrayList<Cache> aux=basedados.getBaseRegiao().get(regiao);
        if(!basedados.getBaseRegiao().containsKey(regiao)){ 
            System.out.println("Esta regi�o n�o tem caches criadas");
            mostraMensagem();
            execMenuProcurarCache(u);
        }  
        else{
            for(Cache c:aux){
                if(flag==0){
                    execMenuProcurarCache(u);
                }else{
                    flag=execMenuDecProcuraR(u,c);   
                }
            }
            if(flag==0){execMenuProcurarCache(u);}
            else{
                System.out.println("N�o existem mais caches nesta regi�o");
                mostraMensagem();
                execMenuProcurarCache(u);
            }
        }
    }

    public static int execMenuDecProcuraU(String u,Cache c){
        int op;
        do{
            System.out.println(c.toString());
            menuDecProcura.executa();
            switch (op=menuDecProcura.getOpcao())  {
                case 1: execMenuRegistarCache(u,c);
                break;
                case 2: clean();
                break;
            }
        }while (menuDecProcura.getOpcao() !=0 && menuDecProcura.getOpcao() !=2);
        clean();
        return op;
    }

    public static int execMenuDecProcuraP(String u,Cache c){ 
        int op;
        do{
            System.out.println("Dono: "+c.getDono());
            System.out.println(c.toString());
            menuDecProcura.executa();
            switch (op=menuDecProcura.getOpcao())  {
                case 1: execMenuRegistarCache(u,c);
                break;
                case 2: clean();
                break;
            }

        }while (menuDecProcura.getOpcao() !=0 && menuDecProcura.getOpcao() !=2);
        clean();
        return op;
    }

    public static int execMenuDecProcuraR(String u,Cache c){
        int op;
        do{
            System.out.println("Dono: "+c.getDono());
            System.out.println(c.toString());
            menuDecProcura.executa();
            switch (op=menuDecProcura.getOpcao())  {
                case 1: execMenuRegistarCache(u,c);
                break;
                case 2: clean();
                break;
            }

        }while (menuDecProcura.getOpcao() !=0 && menuDecProcura.getOpcao() !=2);
        clean();
        return op;
    }

    public static void execMenuRegistarCache(String u,Cache c){ 
        do {
            menuRegCache.executa(); 
            switch (menuRegCache.getOpcao()) {
                case 1: regComMet(u,c);
                break;

                case 2: regSemMet(u,c);
                break;
            }
        }while (menuRegCache.getOpcao()!=0);
    }

    public static void regComMet(String u,Cache c){
        TreeSet<Actividade> atividadesAux = rede.getUser(u).getListaActividades();
        Usuario usuarioAux = rede.getUser(u);
        String dono = c.getDono();
        String tipoC = c.getClass().getName();
        GregorianCalendar data = new GregorianCalendar();
        ArrayList<String> objetos=new ArrayList<>();
        ArrayList<String> livroReg = c.getLivroReg();
        String tipo;
        switch(tipoC = c.getClass().getName()){
            case "Micro":
            objetos.add("O tipo de cache nao permitia objetos"); //cache micro nao contem objetos
            // verificar coordenadas
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                //verificar posteriormente se o livro de registos tem o email do usuario
                if(!livroReg.contains(u)){
                    String periodoDia ;
                    String ceu;
                    String tempo;
                    int pontos;
                    int temperatura;
                    int vento;
                    int humidade;
                    int distancia;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    System.out.println("Em que periodo foi encontrou a cache ? (madrugada,manha,tarde ou noite)");
                    while(true){
                        periodoDia = Input.lerString();
                        if(periodoDia.equals("madrugada") || periodoDia.equals("manha") || periodoDia.equals("tarde") || periodoDia.equals("noite")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o ceu ? (limpo,nublado ou muito nublado)");
                    while(true){
                        ceu = Input.lerString();
                        if(ceu.equals("limpo") || ceu.equals("nublado") || ceu.equals("muito nublado")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o tempo ? (sem chuva, pouca chuva ou muita chuva)");
                    while(true){
                        tempo = Input.lerString();
                        if(tempo.equals("sem chuva") || tempo.equals("pouca chuva") || tempo.equals("muita chuva")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Que temperatura estava ?");
                    temperatura = Input.lerInt();
                    System.out.println("Vento em Km/hora ?");
                    vento = Input.lerInt();
                    System.out.println("Percentagem de humidade ?");
                    humidade = Input.lerInt();
                    Meteorologia met = new Meteorologia(periodoDia,ceu,tempo,temperatura,vento,humidade);
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }else System.out.println("Tem de visitar o sitio para poder registar");
            break;
            
            case "Virtual":
            objetos.add("O tipo de cache nao permitia objetos"); //cache micro nao contem objetos
            // verificar coordenadas
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                //verificar posteriormente se o livro de registos tem o email do usuario
                if(!livroReg.contains(u)){
                    String periodoDia ;
                    String ceu;
                    String tempo;
                    int pontos;
                    int temperatura;
                    int vento;
                    int humidade;
                    int distancia;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    System.out.println("Em que periodo foi encontrou a cache ? (madrugada,manha,tarde ou noite)");
                    while(true){
                        periodoDia = Input.lerString();
                        if(periodoDia.equals("madrugada") || periodoDia.equals("manha") || periodoDia.equals("tarde") || periodoDia.equals("noite")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o ceu ? (limpo,nublado ou muito nublado)");
                    while(true){
                        ceu = Input.lerString();
                        if(ceu.equals("limpo") || ceu.equals("nublado") || ceu.equals("muito nublado")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o tempo ? (sem chuva, pouca chuva ou muita chuva)");
                    while(true){
                        tempo = Input.lerString();
                        if(tempo.equals("sem chuva") || tempo.equals("pouca chuva") || tempo.equals("muita chuva")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Que temperatura estava ?");
                    temperatura = Input.lerInt();
                    System.out.println("Vento em Km/hora ?");
                    vento = Input.lerInt();
                    System.out.println("Percentagem de humidade ?");
                    humidade = Input.lerInt();
                    Meteorologia met = new Meteorologia(periodoDia,ceu,tempo,temperatura,vento,humidade);
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }else System.out.println("Tem de visitar o sitio para poder registar");
            break;
            
            case "Evento":
            objetos.add("O tipo de cache nao permitia objetos"); //cache micro nao contem objetos
            // verificar coordenadas
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                //verificar posteriormente se o livro de registos tem o email do usuario
                if(!livroReg.contains(u)){
                    String periodoDia ;
                    String ceu;
                    String tempo;
                    int pontos;
                    int temperatura;
                    int vento;
                    int humidade;
                    int distancia;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    System.out.println("Em que periodo foi encontrou a cache ? (madrugada,manha,tarde ou noite)");
                    while(true){
                        periodoDia = Input.lerString();
                        if(periodoDia.equals("madrugada") || periodoDia.equals("manha") || periodoDia.equals("tarde") || periodoDia.equals("noite")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o ceu ? (limpo,nublado ou muito nublado)");
                    while(true){
                        ceu = Input.lerString();
                        if(ceu.equals("limpo") || ceu.equals("nublado") || ceu.equals("muito nublado")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o tempo ? (sem chuva, pouca chuva ou muita chuva)");
                    while(true){
                        tempo = Input.lerString();
                        if(tempo.equals("sem chuva") || tempo.equals("pouca chuva") || tempo.equals("muita chuva")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Que temperatura estava ?");
                    temperatura = Input.lerInt();
                    System.out.println("Vento em Km/hora ?");
                    vento = Input.lerInt();
                    System.out.println("Percentagem de humidade ?");
                    humidade = Input.lerInt();
                    Meteorologia met = new Meteorologia(periodoDia,ceu,tempo,temperatura,vento,humidade);
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }else System.out.println("Tem de visitar o sitio para poder registar");
            break;

            case "Misterio":
            ArrayList<String> objetosmist=new ArrayList<>();
            Coordenadas finais = new Coordenadas(((Misterio)c).getLatitudeF(),((Misterio)c).getLongitudeF());
            if(!finais.equals(usuarioAux.getUltimaVisita())){
                if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                    if(!livroReg.contains(u)){
                        System.out.println("Pergunta secreta: "+((Misterio)c).getPergunta());
                        System.out.println("Insira a resposta ");
                        String resposta;
                        String respostaCerta = ((Misterio)c).getResposta();
                        while(true){
                            resposta = Input.lerString();
                            if(resposta.equals(respostaCerta)){
                                break;
                            }else System.out.println("Resposta errada, tente outra vez");
                        }
                        System.out.println("Resposta correta as coordenadas finais para encontrar a cache s�o: "+finais);
                        mostraMensagem();
                        execMenuUsuario(u);
                    }else System.out.println("Email j� existe no livro de registo");
                }else System.out.println("Tem de visitar o sitio para poder registar");
            }else{
                if(!livroReg.contains(u)){
                    objetos=((Misterio)c).getObjetos();
                    if(!objetos.isEmpty()){
                        int cont=0;
                        System.out.println("Deseja retirar algum objeto ? (sim/n�o)");
                        String dec;
                        while(true){
                            dec=Input.lerString();
                            if(dec.equals("sim")){
                                for(String a:objetos){
                                    System.out.println(cont);
                                    System.out.println(a.toString());
                                    cont++;
                                }
                                System.out.println("Insira o n� do objeto que quer retirar");
                                int pos = Input.lerInt();
                                objetosmist.add(objetos.get(pos));
                                objetos.remove(pos);
                                break;
                            }
                            if(dec.equals("n�o")){ 
                                break;
                            }else {System.out.println("Op��o inv�lida, insira apenas sim ou n�o");}
                        }
                    }else objetosmist.add("A cache n�o contem objetos");
                    String periodoDia;
                    String ceu;
                    String tempo;
                    int pontos;
                    int temperatura;
                    int vento;
                    int humidade;
                    int distancia;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    System.out.println("Em que periodo foi encontrou a cache ? (madrugada,manha,tarde ou noite)");
                    while(true){
                        periodoDia = Input.lerString();
                        if(periodoDia.equals("madrugada") || periodoDia.equals("manha") || periodoDia.equals("tarde") || periodoDia.equals("noite")) break;
                        else System.out.println("op�ao nao existe");
                    }
                    System.out.println("Como estava o ceu ? (limpo,nublado ou muito nublado)");
                    while(true){
                        ceu = Input.lerString();
                        if(ceu.equals("limpo") || ceu.equals("nublado") || ceu.equals("muito nublado")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o tempo ? (sem chuva, pouca chuva ou muita chuva)");
                    while(true){
                        tempo = Input.lerString();
                        if(tempo.equals("sem chuva") || tempo.equals("pouca chuva") || tempo.equals("muita chuva")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Que temperatura estava ?");
                    temperatura = Input.lerInt();
                    System.out.println("Vento em Km/hora ?");
                    vento = Input.lerInt();
                    System.out.println("Percentagem de humidade ?");
                    humidade = Input.lerInt();
                    Meteorologia met = new Meteorologia(periodoDia,ceu,tempo,temperatura,vento,humidade);
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }
            }
            break;
            case "Multi":
            ArrayList<String> objetosmul=new ArrayList<>();
            // verificar coordenadas
            if(!((Multi)c).getCoords().get(((Multi)c).getCoords().size()-1).equals(usuarioAux.getUltimaVisita())){
                if(((Multi)c).getCoords().contains(usuarioAux.getUltimaVisita())){
                    if(!livroReg.contains(u)){
                        System.out.println("Ainda nao encontrou a cache, visite o ponto de coordenadas :"+proxC(u,c));
                    }else System.out.println("Email j� existe no livro de registo");
                }else System.out.println("Tem de visitar o sitio para poder registar");
            }else{
                if(!livroReg.contains(u)){
                    objetos=((Multi)c).getObjetos();
                    if(!objetos.isEmpty()){
                        int cont=0;
                        System.out.println("Deseja retirar algum objeto ? (sim/n�o)");
                        String dec;
                        while(true){
                            dec=Input.lerString();
                            if(dec.equals("sim")){
                                for(String a:objetos){
                                    System.out.println(cont);
                                    System.out.println(a.toString());
                                    cont++;
                                }
                                System.out.println("Insira o n� do objeto que quer retirar");
                                int pos = Input.lerInt();
                                objetosmul.add(objetos.get(pos));
                                objetos.remove(pos);
                                break;
                            }
                            if(dec.equals("n�o")){ 
                                break;
                            }else {System.out.println("Op��o inv�lida, insira apenas sim ou n�o");}
                        }
                    }else objetosmul.add("A cache n�o contem objetos");
                    String periodoDia;
                    String ceu;
                    String tempo;
                    int pontos;
                    int temperatura;
                    int vento;
                    int humidade;
                    int distancia;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    System.out.println("Em que periodo foi encontrou a cache ? (madrugada,manha,tarde ou noite)");
                    while(true){
                        periodoDia = Input.lerString();
                        if(periodoDia.equals("madrugada") || periodoDia.equals("manha") || periodoDia.equals("tarde") || periodoDia.equals("noite")) break;
                        else System.out.println("op�ao nao existe");
                    }
                    System.out.println("Como estava o ceu ? (limpo,nublado ou muito nublado)");
                    while(true){
                        ceu = Input.lerString();
                        if(ceu.equals("limpo") || ceu.equals("nublado") || ceu.equals("muito nublado")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Como estava o tempo ? (sem chuva, pouca chuva ou muita chuva)");
                    while(true){
                        tempo = Input.lerString();
                        if(tempo.equals("sem chuva") || tempo.equals("pouca chuva") || tempo.equals("muita chuva")) break;
                        else System.out.println("op�ao nao existe");
                    }

                    System.out.println("Que temperatura estava ?");
                    temperatura = Input.lerInt();
                    System.out.println("Vento em Km/hora ?");
                    vento = Input.lerInt();
                    System.out.println("Percentagem de humidade ?");
                    humidade = Input.lerInt();
                    Meteorologia met = new Meteorologia(periodoDia,ceu,tempo,temperatura,vento,humidade);
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }
            break;
            case "Tradicional":
            ArrayList<String> objetostra=new ArrayList<>();
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                if(!livroReg.contains(u)){
                    objetos=((Tradicional)c).getObjetos();
                    if(!objetos.isEmpty()){
                        int cont=0;
                        System.out.println("Deseja retirar algum objeto ? (sim/n�o)");
                        String dec;
                        while(true){
                            dec=Input.lerString();
                            if(dec.equals("sim")){
                                for(String a:objetos){
                                    System.out.println(cont);
                                    System.out.println(a.toString());
                                    cont++;
                                }
                                System.out.println("Insira o n� do objeto que quer retirar");
                                int pos = Input.lerInt();
                                objetostra.add(objetos.get(pos));
                                objetos.remove(pos);
                                break;
                            }
                            if(dec.equals("n�o")){ 
                                break;
                            }else {System.out.println("Op��o inv�lida, insira apenas sim ou n�o");}
                        }
                    }else objetostra.add("A cache n�o contem objetos");
                    String periodoDia;
                    String ceu;
                    String tempo;
                    int pontos;
                    int temperatura;
                    int vento;
                    int humidade;
                    int distancia;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    System.out.println("Em que periodo foi encontrou a cache ? (madrugada,manha,tarde ou noite)");
                    while(true){
                        periodoDia = Input.lerString();
                        if(periodoDia.equals("madrugada") || periodoDia.equals("manha") || periodoDia.equals("tarde") || periodoDia.equals("noite")) break;
                        else System.out.println("Op��o n�o existe");
                    }

                    System.out.println("Como estava o ceu ? (limpo,nublado ou muito nublado)");
                    while(true){
                        ceu = Input.lerString();
                        if(ceu.equals("limpo") || ceu.equals("nublado") || ceu.equals("muito nublado")) break;
                        else System.out.println("Op��o n�o existe");
                    }

                    System.out.println("Como estava o tempo ? (sem chuva, pouca chuva ou muita chuva)");
                    while(true){
                        tempo = Input.lerString();
                        if(tempo.equals("sem chuva") || tempo.equals("pouca chuva") || tempo.equals("muita chuva")) break;
                        else System.out.println("Op��o n�o existe");
                    }

                    System.out.println("Que temperatura estava ?");
                    temperatura = Input.lerInt();
                    System.out.println("Vento em Km/hora ?");
                    vento = Input.lerInt();
                    System.out.println("Percentagem de humidade ?");
                    humidade = Input.lerInt();
                    Meteorologia met = new Meteorologia(periodoDia,ceu,tempo,temperatura,vento,humidade);
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);

                }else System.out.println("Email j� existe no livro de registo");
            }System.out.println("Tem de visitar o sitio para poder registar");
            break;
        }
    }
    

    // Se o usuario visitou algum dos pontos intermedios da cache retorna o proximo
    public static Coordenadas proxC(String u,Cache c){
        Usuario aux=rede.getUser(u);
        return ((Multi)c).getCoords().get(((Multi)c).getCoords().indexOf(aux.getUltimaVisita())+1);
    }

    // actualizar estatisticas de um usuario num determinado ano e mes com uma distancia percorrida e pontua�ao

    public static void actualizaE(Usuario u,int ano,int mes,int distancia,int pontos){
        //caso nao haja estatisticas para o determinado ano , cria-se um arrayList com 12 estatisticas em "default"
        // e actualiza-se a estatistica do mes recebido
        // "Default" -> caches criadas=0 ; caches encontradas=0 ; distancia percorida = 0 ; pontua�ao total = 0;
        HashMap <Integer,ArrayList<Estatistica>> hashAux=u.getEstatisticas();
        Estatistica aux=new Estatistica();
        ArrayList<Estatistica> listavazia=new ArrayList<>();
        for(int i=0;i<12;i++){
            aux=new Estatistica();
            listavazia.add(aux.clone());
        }

        if(!u.getEstatisticas().containsKey(ano)){
            //actualizar estado da estatistica do mes dado no ano dado
            aux.setCachesCriadas(1);
            aux.setCachesEncontradas(1);
            aux.setDPercorrida(distancia);
            aux.setPontosTotais(pontos);
            listavazia.set(mes,aux); //colocar na listavazia de estatisticas a primeira actualiza�ao de estatistica , neste caso o aux
            hashAux.put(ano,listavazia); //inserir na hash auxiliar
            u.setEstatisticas(hashAux); // repor na hash do usuario , como hashAux so foi criada aqui e desaparece quando finaliza a funcao o set � feito sem clonar nada ja que a hashAux j� � um clone
        }
        else{
            hashAux.get(ano).get(mes).adicionaValores(distancia,pontos); //procura a estatistica do mes e ano dados e actualiza
            u.setEstatisticas(hashAux);  // repoem no hashmap do usuario a nova hash com valores actualizados 

        }
    }

    public static void regSemMet(String u,Cache c){
       TreeSet<Actividade> atividadesAux = rede.getUser(u).getListaActividades();
        Usuario usuarioAux = rede.getUser(u);
        String dono = c.getDono();
        String tipoC = c.getClass().getName();
        GregorianCalendar data = new GregorianCalendar();
        ArrayList<String> objetos=new ArrayList<>();
        ArrayList<String> livroReg = c.getLivroReg();
        String tipo;
        switch(tipoC = c.getClass().getName()){
            case "Micro":
            objetos.add("O tipo de cache nao permitia objetos"); //cache micro nao contem objetos
            // verificar coordenadas
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                //verificar posteriormente se o livro de registos tem o email do usuario
                if(!livroReg.contains(u)){
                    int pontos;
                    int distancia;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    Meteorologia met = new Meteorologia();
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }else System.out.println("Tem de visitar o sitio para poder registar");
            break;
            
            case "Virtual":
            objetos.add("O tipo de cache nao permitia objetos"); //cache micro nao contem objetos
            // verificar coordenadas
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                //verificar posteriormente se o livro de registos tem o email do usuario
                if(!livroReg.contains(u)){
                    int distancia;
                    int pontos;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    Meteorologia met = new Meteorologia();
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }else System.out.println("Tem de visitar o sitio para poder registar");
            break;
            
            case "Evento":
            objetos.add("O tipo de cache nao permitia objetos"); //cache micro nao contem objetos
            // verificar coordenadas
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                //verificar posteriormente se o livro de registos tem o email do usuario
                if(!livroReg.contains(u)){
                    int distancia;
                    int pontos;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    Meteorologia met = new Meteorologia();
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }else System.out.println("Tem de visitar o sitio para poder registar");
            break;

            case "Misterio":
            ArrayList<String> objetosmist=new ArrayList<>();
            Coordenadas finais = new Coordenadas(((Misterio)c).getLatitudeF(),((Misterio)c).getLongitudeF());
            if(!finais.equals(usuarioAux.getUltimaVisita())){
                if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                    if(!livroReg.contains(u)){
                        System.out.println("Pergunta secreta: "+((Misterio)c).getPergunta());
                        System.out.println("Insira a resposta ");
                        String resposta;
                        String respostaCerta = ((Misterio)c).getResposta();
                        while(true){
                            resposta = Input.lerString();
                            if(resposta.equals(respostaCerta)){
                                break;
                            }else System.out.println("Resposta errada, tente outra vez");
                        }
                        System.out.println("Resposta correta as coordenadas finais para encontrar a cache s�o: "+finais);
                        mostraMensagem();
                        execMenuUsuario(u);
                    }else System.out.println("Email j� existe no livro de registo");
                }else System.out.println("Tem de visitar o sitio para poder registar");
            }else{
                if(!livroReg.contains(u)){
                    objetos=((Misterio)c).getObjetos();
                    if(!objetos.isEmpty()){
                        int cont=0;
                        System.out.println("Deseja retirar algum objeto ? (sim/n�o)");
                        String dec;
                        while(true){
                            dec=Input.lerString();
                            if(dec.equals("sim")){
                                for(String a:objetos){
                                    System.out.println(cont);
                                    System.out.println(a.toString());
                                    cont++;
                                }
                                System.out.println("Insira o n� do objeto que quer retirar");
                                int pos = Input.lerInt();
                                objetosmist.add(objetos.get(pos));
                                objetos.remove(pos);
                                break;
                            }
                            if(dec.equals("n�o")){ 
                                break;
                            }else {System.out.println("Op��o inv�lida, insira apenas sim ou n�o");}
                        }
                    }else objetosmist.add("A cache n�o contem objetos");
                    int distancia;
                    int pontos;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    Meteorologia met = new Meteorologia();
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }
            }
            break;
            case "Multi":
            ArrayList<String> objetosmul=new ArrayList<>();
            // verificar coordenadas
            if(!((Multi)c).getCoords().get(((Multi)c).getCoords().size()-1).equals(usuarioAux.getUltimaVisita())){
                if(((Multi)c).getCoords().contains(usuarioAux.getUltimaVisita())){
                    if(!livroReg.contains(u)){
                        System.out.println("Ainda nao encontrou a cache, visite o ponto de coordenadas :"+proxC(u,c));
                    }else System.out.println("Email j� existe no livro de registo");
                }else System.out.println("Tem de visitar o sitio para poder registar");
            }else{
                if(!livroReg.contains(u)){
                    objetos=((Multi)c).getObjetos();
                    if(!objetos.isEmpty()){
                        int cont=0;
                        System.out.println("Deseja retirar algum objeto ? (sim/n�o)");
                        String dec;
                        while(true){
                            dec=Input.lerString();
                            if(dec.equals("sim")){
                                for(String a:objetos){
                                    System.out.println(cont);
                                    System.out.println(a.toString());
                                    cont++;
                                }
                                System.out.println("Insira o n� do objeto que quer retirar");
                                int pos = Input.lerInt();
                                objetosmul.add(objetos.get(pos));
                                objetos.remove(pos);
                                break;
                            }
                            if(dec.equals("n�o")){ 
                                break;
                            }else {System.out.println("Op��o inv�lida, insira apenas sim ou n�o");}
                        }
                    }else objetosmul.add("A cache n�o contem objetos");
                    int distancia;
                    int pontos;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    Meteorologia met = new Meteorologia();
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);
                }else System.out.println("Email j� existe no livro de registo");
            }
            break;
            case "Tradicional":
            ArrayList<String> objetostra=new ArrayList<>();
            if(usuarioAux.getUltimaVisita().equals(c.getLatLong())){
                if(!livroReg.contains(u)){
                    objetos=((Tradicional)c).getObjetos();
                    if(!objetos.isEmpty()){
                        int cont=0;
                        System.out.println("Deseja retirar algum objeto ? (sim/n�o)");
                        String dec;
                        while(true){
                            dec=Input.lerString();
                            if(dec.equals("sim")){
                                for(String a:objetos){
                                    System.out.println(cont);
                                    System.out.println(a.toString());
                                    cont++;
                                }
                                System.out.println("Insira o n� do objeto que quer retirar");
                                int pos = Input.lerInt();
                                objetostra.add(objetos.get(pos));
                                objetos.remove(pos);
                                break;
                            }
                            if(dec.equals("n�o")){ 
                                break;
                            }else {System.out.println("Op��o inv�lida, insira apenas sim ou n�o");}
                        }
                    }else objetostra.add("A cache n�o contem objetos");
                    int distancia;
                    int pontos;
                    int ano=data.get(GregorianCalendar.YEAR);
                    int mes=data.get(GregorianCalendar.MONTH);
                    System.out.println("Qual foi em Km a distancia percorrida ate encontrar a cache ?");
                    distancia=Input.lerInt();
                    Meteorologia met = new Meteorologia();
                    Actividade actividade = new Actividade(dono,tipoC,c.getLatLong(),met,objetos,distancia);
                    pontos=actividade.calcPontosA();
                    actividade.setPontos(pontos);
                    usuarioAux.addActividade(actividade);
                    actualizaE(usuarioAux,ano,mes,distancia,pontos);
                    livroReg.add(u);
                    rede.replaceUsuario(usuarioAux);
                    System.out.println("Actividade registada com sucesso");
                    mostraMensagem();
                    execMenuUsuario(u);

                }else System.out.println("Email j� existe no livro de registo");
            }System.out.println("Tem de visitar o sitio para poder registar");
            break;
        }
    }

    public static void criarMicro(String u){
        Usuario usuarioAux = rede.getUser(u);
        System.out.println("Insira a latitude");
        double latitude=Input.lerDouble();
        System.out.println("Insira a longitude");
        double longitude=Input.lerDouble();
        System.out.println("Insira informa��o sobre a cache");
        String info=Input.lerString();
        int idAux=id;
        Micro micro = new Micro(latitude,longitude,info,usuarioAux.getEmail(),idAux);
        id++;
        try{
            usuarioAux.addCache(micro);
        }
        catch(CacheExisteException e){
            System.out.println("Cache j� existe");
            mostraMensagem();
            execMenuCriarCache(u);
        }
        rede.replaceUsuario(usuarioAux);
        basedados.addBaseDados(usuarioAux);
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void criarVirtual(String u){
        Usuario usuarioAux = rede.getUser(u);
        System.out.println("Insira a latitude");
        double latitude=Input.lerDouble();
        System.out.println("Insira a longitude");
        double longitude=Input.lerDouble();
        System.out.println("Insira informa��o sobre a cache");
        String info=Input.lerString();
        int idAux=id;
        Virtual virtual = new Virtual(latitude,longitude,info,usuarioAux.getEmail(),idAux);
        id++;
        try{
            usuarioAux.addCache(virtual);
        }
        catch(CacheExisteException e){
            System.out.println("Cache j� existe");
            mostraMensagem();
            execMenuCriarCache(u);
        }
        rede.replaceUsuario(usuarioAux);
        basedados.addBaseDados(usuarioAux);
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void criarEvento(String u){
        Usuario usuarioAux = rede.getUser(u);
        System.out.println("Insira a latitude");
        double latitude=Input.lerDouble();
        System.out.println("Insira a longitude");
        double longitude=Input.lerDouble();
        System.out.println("Insira informa��o sobre a cache");
        String info=Input.lerString();
        int idAux=id;
        Evento evento = new Evento(latitude,longitude,info,usuarioAux.getEmail(),idAux);
        id++;
        try{
            usuarioAux.addCache(evento);
        }
        catch(CacheExisteException e){
            System.out.println("Cache j� existe");
            mostraMensagem();
            execMenuCriarCache(u);
        }
        rede.replaceUsuario(usuarioAux);
        basedados.addBaseDados(usuarioAux);
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void criarTradicional(String u){
        Usuario aux = rede.getUser(u);
        System.out.println("Insira a latitude");
        double latitude=Input.lerDouble();
        System.out.println("Insira a longitude");
        double longitude=Input.lerDouble();
        System.out.println("Quantos objetos vai inserir na cache ?");
        int nobj=Input.lerInt();
        ArrayList<String> obj = new ArrayList<String>();
        for(int i=0;i<nobj;i++){
            System.out.println("Insira o "+(i+1)+"� objeto");
            obj.add(Input.lerString());
        }
        System.out.println("Insira a informa��o da cache");
        String info=Input.lerString();
        Tradicional tradicional = new Tradicional(latitude,longitude,info,obj,aux.getEmail(),id);
        id++;
        try{
            aux.addCache(tradicional);
        }
        catch(CacheExisteException e){
            System.out.println("Cache j� existe");
            mostraMensagem();
            execMenuCriarCache(u);
        }
        rede.replaceUsuario(aux);
        basedados.addBaseDados(aux);
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void criarMisterio(String u){
        Usuario aux = rede.getUser(u);
        System.out.println("Insira a latitude inicial");
        double latitudeI=Input.lerDouble();
        System.out.println("Insira a longitude inicial");
        double longitudeI=Input.lerDouble();
        System.out.println("Insira a latitude final");
        double latitudeF=Input.lerDouble();
        System.out.println("Insira a longitude final");
        double longitudeF=Input.lerDouble();
        System.out.println("Insira a informa��o da cache");
        String info=Input.lerString();
        System.out.println("Insira a pergunta secreta");
        String pergunta = Input.lerString();
        System.out.println("Insira a resposta");
        String resposta = Input.lerString();
        System.out.println("Quantos objetos vai inserir na cache ?");
        int nobj=Input.lerInt();
        ArrayList<String> obj = new ArrayList<String>();
        for(int i=0;i<nobj;i++){
            System.out.println("Insira o "+(i+1)+"� objeto");
            obj.add(Input.lerString());
        }
        Misterio misterio = new Misterio(latitudeI,longitudeI,latitudeF,longitudeF,info,pergunta,resposta,aux.getEmail(),id,obj);
        id++;
        try{
            aux.addCache(misterio);
        }
        catch(CacheExisteException e){
            System.out.println("Cache j� existe");
            mostraMensagem();
            execMenuCriarCache(u);
        }
        rede.replaceUsuario(aux);
        basedados.addBaseDados(aux);
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void criarMulti(String u){
        Usuario aux = rede.getUser(u);
        int nCoords,i;
        ArrayList<Coordenadas> aux2=new ArrayList<>();
        Coordenadas coord = new Coordenadas();
        System.out.println("Insira o n� total de pontos que a cache tem desde o ponto inicial at� ao ponto final");
        nCoords=Input.lerInt();
        System.out.println("Insira os pontos  separadas com 'enters'");
        for(i=0;i<nCoords;i++){
            System.out.println("insira o "+(i+1)+"� ponto");
            coord.setLatitude(Input.lerDouble());
            coord.setLongitude(Input.lerDouble());
            aux2.add(coord.clone());
        }
        double latitude=aux2.get(0).getLatitude();
        double longitude=aux2.get(0).getLongitude();
        System.out.println("Insira a informa��o da cache");
        String info=Input.lerString();
        System.out.println("Quantos objetos vai inserir na cache ?");
        int nobj=Input.lerInt();
        ArrayList<String> obj = new ArrayList<String>();
        for(i=0;i<nobj;i++){
            System.out.println("Insira o "+(i+1)+"� objeto");
            obj.add(Input.lerString());
        }
        Multi multi = new Multi(latitude,longitude,info,aux.getEmail(),id,aux2,obj);
        id++;
        try{
            aux.addCache(multi);
        }
        catch(CacheExisteException e){
            System.out.println("Cache j� existe");
            mostraMensagem();
            execMenuCriarCache(u);
        }
        rede.replaceUsuario(aux);
        basedados.addBaseDados(aux);
        mostraMensagem();
        execMenuUsuario(u);
    }

    public static void visitar(String u){
        Usuario aux = rede.getUser(u);
        Coordenadas c = new Coordenadas();
        System.out.println("Insira a latitude que quer visitar");
        double latitude = Input.lerDouble();
        System.out.println("Insira a longitude que quer visitar");
        double longitude = Input.lerDouble();
        c.setCoordenadas(latitude,longitude);
        aux.setUltimaVisita(c);
        Calendar horaUV = Calendar.getInstance();
        aux.setHoraUltimaVisita(horaUV);
        rede.replaceUsuario(aux);
    }
    
    public static void simularEvento(){
        //numero de participantes
        int nparticipantes;
        //ArrayList com os participantes
        ArrayList<Usuario> participantes=new ArrayList<>();
        //Copia da rede de Usuarios
        Set<Map.Entry<String,Usuario>> copiaRedeUsuario=rede.getRede().entrySet();
        Iterator itr=copiaRedeUsuario.iterator();
        //Numero aleatorio de participantes entre 0 e 15
        Random rnp=new Random();
        nparticipantes=rnp.nextInt(16);
        //caso o numero de participantes seja <2 volta a gerar outro inteiro
        while(nparticipantes<2){
            nparticipantes=rnp.nextInt(16);
        }
        //passar n participantes para o array auxiliar
        while(itr.hasNext() && nparticipantes >0){
            Map.Entry<String,Usuario> thisEntryr =(Map.Entry)itr.next();
            String key = thisEntryr.getKey();
            Usuario usuarioAux = thisEntryr.getValue();
            participantes.add(usuarioAux);
            nparticipantes--;
        
        }
        
        int ncaches;
        //ArrayList com as caches a encontrar
        ArrayList<Cache> cachesevento =new ArrayList<>();
        //copia da base de dados das caches
        Set<Map.Entry<String,ArrayList<Cache>>> copiaBaseDados=basedados.getBaseUsuario().entrySet();
        Iterator itc=copiaBaseDados.iterator();
        //Numero aleatorio de caches entre 15 e 30
        Random rnc=new Random();
        ncaches=rnc.nextInt(31);
        //caso o numero de caches seja <15 volta a gerar outro inteiro
        while(ncaches<15){
            ncaches=rnc.nextInt(31);
        }
        //passar n caches para o array auxiliar
        while(itc.hasNext() && ncaches >0){
            Map.Entry<String,ArrayList<Cache>> thisEntryc =(Map.Entry)itc.next();
            String chave = thisEntryc.getKey();
            ArrayList<Cache> listaCache = thisEntryc.getValue();
            copiaArray(cachesevento,listaCache);
            ncaches--;
        
        }
   
        //Randomicamente troca os usuarios de posi�ao
        Collections.shuffle(participantes);
        //Randomicamente troca os usuarios de posi�ao
        Collections.shuffle(cachesevento);
        //escolhe uma posi�ao aleatoria do array 
        int escolha;
        Random escolhar=new Random(); 
        int iteracoes; //numero de 'ticks' na simula�ao , cada tick � a escolha de um usuario de uma cache e o registo da mesma
        iteracoes=escolhar.nextInt(20); //pode se mudar o numero de itera�oes manualmente no menu (por implementar)
        
        //Cache
        Cache cache;
        String tipo;
        
        //Usuario
        
        Usuario cacher;
        
        //array com 'objecto'
        ArrayList<String> obj=new ArrayList<>();
        obj.add("Cache nao continha objetos");
        // arrays e inteiros para a meteorologia

        String[] periodoDia= {"madrugada","manha","tarde","noite"};
        String[] ceu= {"limpo","nublado","muito nublado"};
        String[] tempo= {"sem chuva","pouca chuva","muita chuva"};
        
        int temperatura;
        int vento;
        int humidade;
        int distancia;
        
        //Strings de escolha
        String pD;
        String c;
        String t;
        
        //Meteorologia
        Meteorologia randomMeteo;
        
        //pontos
        int pontos;
        while(iteracoes>0){
            //Escolher entre as op�oes e guardar
        escolha=escolhar.nextInt(4);
        pD=periodoDia[escolha];
        escolha=escolhar.nextInt(3);
        c=ceu[escolha];
        escolha=escolhar.nextInt(3);
        t=tempo[escolha];
        
        //Fim das escolhas aleatorias de arrays
        
        //Escolher os inteiros
        escolha=escolhar.nextInt(100);
        temperatura=escolha;
        escolha=escolhar.nextInt(100);
        vento=escolha;
        escolha=escolhar.nextInt(100);
        humidade=escolha;
        escolha=escolhar.nextInt(100);
        distancia=escolha;
        
        //Fim das escolhas aleatorias de inteiros
        
        //Criar meteorologia com parametros dados
        
        randomMeteo=new Meteorologia(pD,c,t,temperatura,vento,humidade);
        
        //Escolher uma cache da lista
        
        escolha=escolhar.nextInt(cachesevento.size());
        
        
        cache=cachesevento.get(escolha); //guardar cache
        
        escolha=escolhar.nextInt(participantes.size());
        cacher=participantes.get(escolha);  // guardar o usuario
        cacher.setUltimaVisita(cache.getLatLong());
        
        System.out.println("O usuario :"+cacher.getNome()+" esta a procura da cache :"+cache.toString());
        
            
        Actividade atAux=new Actividade(cache.getDono(),cache.getClass().getName(),cache.getLatLong(),randomMeteo,obj,distancia);
        pontos=atAux.calcPontosA();
        atAux.setPontos(pontos);
        cacher.addActividade(atAux);
        System.out.println("O usuario :"+cacher.getNome()+" encontrou e registou a cache "+cacher.getListaActividades());
        actualizaE(cacher,2015,7,distancia,pontos);
        System.out.println(cacher.getEstatisticas().get(2015).get(7).toString());
        iteracoes--;
        
       }
       mostraMensagem();
        
        
        
    }
    
      public static ArrayList<Cache> copiaArray(ArrayList<Cache> a1,ArrayList<Cache> a2){
        for(Cache c:a2){
            a1.add(c);
        }
        return a1;
    }

    public static void execMenuEditar(String u){
        do{
            clean();
            menuEditar.executa();  
            switch (menuEditar.getOpcao()){
                case 1: editarMail(u);
                break;
                /*case 2: editarPass(u);
                break;
                case 3: editarNome(u);
                break;
                case 4: editarGenero(u);
                break;
                case 5: editarNome(u);
                break;
                case 6: editarPais(u);
                break;
                case 7: editarRegiao(u);
                break;
                case 8: editarCidade(u);
                break;    
                case 8: editarData(u);
                break;*/      
            }
        }while(menuEditar.getOpcao()!=0);
        clean();
        execMenuUsuario(u); 
    }

    public static void editarMail(String u){
        Usuario aux = new Usuario(rede.getUser(u));
        System.out.println("Insira o novo email");
        String email = Input.lerString();
        System.out.println("Insira novamente o novo email");
        String email2 = Input.lerString();
        if(email==email2){rede.replaceUsuario(aux);}
        else{System.out.println("Os dois emails que inseriu n�o coincidem");}
    }

    public static void carregaMenus(){
        String[] login={    "Login",
                "Criar conta",
            };

        String[] usuario={  "Mostrar lista de amigos",
                "Pedidos de amizade",
                "Criar caches",
                "Ver caches criadas",
                "Ver ultimas 10 atividades",
                "Procurar caches",
                "Editar prefil",
                "Visitar",
                "Ver estatisticas"
            };

        String[] admin={  "Simular Evento"
            };
        String[] pedAmiz={  "Ver pedidos",
                "Enviar pedido"
            };

        String[] decPed={   "Aceitar pedido",
                "Rejeitar pedido"
            }; 

        String[] amigo={    "Ver caches criadas",
                "Ver ultimas 10 atividades",
                "Remover amigo",
                "Ver pr�ximo amigo"
            };   

        String[] criarCache={   "Criar micro-cache",
                "Criar cache-tradicional",
                "Criar cache-mist�rio",
                "Criar cache-evento",
                "Criar cache-virtual",
                "Criar cache-multi"
            };

        String[] procurarCache={    "Procurar caches de um usu�rio",
                "Procurar caches num pa�s",
                "Procurar caches numa regiao",
            };

        String[] decProcura={   "Registar cache",
                "Ver proxima cache"
            };

        String[] decCache={ "Apagar cache",
                "Ver proxima cache"
            };

        String[] RegCache={ "Registar com meteorologia",
                "Registar sem meteorologia"
            };

        String[] Editar={   "Alterar o email",
                "Alterar a password ",
                "Alterar o nome",
                "Alterar o genero",
                "Alterar o pa�s",
                "Alterar a regi�o",
                "Alterar a cidade",
                "Alterar a data de nascimento"
            };

        String[] decAtividades={    "Ver detalhes de uma atividade"
            };

        String[] estat={    "Mostar estatisticas anuais",
                "Mostrar estatisticas mensais"
            };

        menuLogin = new Menu(login);
        menuUsuario = new Menu(usuario);
        menuAdmin = new Menu(admin);
        menuPedidoAmizade = new Menu(pedAmiz);
        menuDecPedidoAmizade = new Menu (decPed);
        menuListaAmigos = new Menu(amigo);
        menuCriarCache = new Menu(criarCache);
        menuProcurarCache = new Menu(procurarCache);
        menuDecProcura = new Menu(decProcura);
        menuDecCache = new Menu(decCache);
        menuRegCache = new Menu(RegCache);
        menuEditar = new Menu(Editar);
        menuDecAtividades = new Menu(decAtividades);
        menuEstat = new Menu(estat);
    }

    private final static void clean(){
        System.out.print('\u000C');
    }

    public static void sair(){

        try{
            rede.gravaObj("State");
            basedados.gravaObj("State2");

        }
        catch(IOException e){
            System.out.println("N�o gravou os dados"+e);   
        }
        System.exit(0);
    }

    private static void mostraMensagem(){

        Scanner reader = new Scanner(System.in);
        String s;
        System.out.println("--- Prima 's' para sair ---");
        do {
            s = Input.lerString();
        }
        while(s.endsWith("s") != true);

    }

    public static void carregaDados() throws IOException,ClassNotFoundException{
        FileInputStream door = new FileInputStream("State"); 
        ObjectInputStream reader = new ObjectInputStream(door); 
        if(rede instanceof Rede)
            rede = (Rede) reader.readObject();

    }

    public static void carregaDados2() throws IOException,ClassNotFoundException{
        FileInputStream door = new FileInputStream("State2"); 
        ObjectInputStream reader = new ObjectInputStream(door); 
        if(basedados instanceof BaseDados)
            basedados = (BaseDados) reader.readObject();

    }

}