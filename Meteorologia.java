import java.util.*;
import java.lang.*;
import java.io.*;
public class Meteorologia implements Serializable{
   private String periodoDia; // 1 - madrugada 2 - manha 3 - tarde 4 - noite
   private String ceu; // 1 - limpo  2 - nublado 3 - muito nublado
   private String tempo; //1 - sem chuva 1 -  pouca chuva 3 - muita chuva
   private int temperatura; // Em graus Celcius
   private int vento; // Em Km/h
   private int humidade; // percentagem , aumenta dificuldade  quanto maior for
   
   //Caso as cond. met. forem omitidas cria-se uma "base" com dificuldade normal
   public Meteorologia(){
       periodoDia="tarde";
       ceu="limpo";
       tempo="sol";
       temperatura=25;
       vento=0;
       humidade=0;
    
    }
    public Meteorologia(String periodoDia,String ceu,String tempo,int temperatura,int vento,int humidade){
        this.periodoDia=periodoDia;
        this.ceu=ceu;
        this.tempo=tempo;
        this.temperatura=temperatura;
        this.vento=vento;
        this.humidade=humidade;
    }
    public Meteorologia(Meteorologia m){
        this.periodoDia=m.getPeriodoDia();
        this.ceu=m.getCeu();
        this.tempo=m.getTempo();
        this.temperatura=m.getTemperatura();
        this.vento=m.getVento();
        this.humidade=m.getHumidade();
    }
    
    public String getPeriodoDia(){ return this.periodoDia;}
    public String getCeu(){return this.ceu;}
    public String getTempo(){return this.tempo;}
    public int getTemperatura(){return this.temperatura;}
    public int getVento(){return this.vento;}
    public int getHumidade(){return this.humidade;}
    
    public void setPeriodoDia(String pd){this.periodoDia=pd;}
    public void setCeu(String ceu){this.ceu=ceu;}
    public void setTempo(String tempo){this.tempo=tempo;}
    public void setTemperatura(int temp){this.temperatura=temp;}
    public void setVento(int vento){this.vento=vento;}
    public void setHumidade(int hum){this.humidade=hum;}
    
    //metodos inst
    
    public int calcPontos(){
        int pontos=0;
        switch(this.periodoDia){
            case "madrugada" : pontos=pontos+4;
                break;
            case "manha"     : pontos=pontos+1;
                break;
            case "tarde"     : pontos=pontos+2;
                break;
            case "noite"     : pontos=pontos+5;
                break;
                
        }
        //quando mais nublado menor dificuldade de dia, menos calor e maior dificuldade de noite
        // menor visibilidade 
        if(this.periodoDia.equals("manha") || this.periodoDia.equals("tarde")){
            if(this.ceu.equals("limpo")) pontos=pontos+3;
            else if(this.ceu.equals("nublado")) pontos=pontos+2;
            else pontos=pontos+1;
        }
        else{
            if(this.ceu.equals("limpo")) pontos=pontos+1;
            else if(this.ceu.equals("nublado")) pontos=pontos+2;
            else pontos=pontos+3;
        }
        switch(this.tempo){
            case "sem chuva"      : pontos=pontos+1;
                break;
            case "pouca chuva"    : pontos=pontos+2;
                break;
            case "muita chuva"    : pontos=pontos+5;
                break;
        }
        //calor dificulta mais que frio
        if(this.temperatura>35)pontos=pontos+4;
        else if(this.temperatura>30)pontos=pontos+3;
        else if(this.temperatura<0)pontos=pontos+4;
        else if(this.temperatura<15)pontos=pontos+3;
        else pontos=pontos+1;
        
        //vento aumenta dificuldade
        if(this.vento>100)pontos=pontos+4;
        else if(this.vento>50)pontos=pontos+3;
        else if(this.vento>20) pontos=pontos+2;
        else pontos=pontos+1;
        
        //quanto maior humidade maior dificuldade
        
        if(this.humidade >75) pontos=pontos+5;
        else if(this.humidade >50) pontos=pontos+4;
        else if(this.humidade >25) pontos=pontos+3;
        else if(this.humidade >5) pontos=pontos+2;
        else pontos=pontos+1;
        
        return pontos;
    }
    
    public Meteorologia clone(){return new Meteorologia(this);}
    
    public String toString(){
        StringBuilder s = new StringBuilder(); 
        s.append("Periodo do dia : ");
        s.append(periodoDia);
        s.append(System.lineSeparator());
        s.append("Ceu : ");
        s.append(ceu);
        s.append(System.lineSeparator());
        s.append("Tempo : ");
        s.append(tempo);
        s.append(System.lineSeparator());
        s.append("Temperatuda (�C) : ");
        s.append(temperatura);
        s.append(System.lineSeparator());
        s.append("Vento (Km) : ");
        s.append(vento);
        s.append(System.lineSeparator());
        s.append("Humidade do ar (%) : ");
        s.append(humidade);
        return s.toString();
    }
    
}
