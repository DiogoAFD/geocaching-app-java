import java.lang.*;
import java.util.*;
import java.io.*;
public class Estatistica implements Serializable
{
   private int cachesCriadas;
   private int cachesEncontradas;
   private int dPercorrida;// Km
   private int pontosTotais;
   
   public Estatistica(){
    cachesCriadas=0;
    cachesEncontradas=0;
    dPercorrida=0;
    pontosTotais=0;
    }
    
    public Estatistica(int cc,int ce,int dp,int pt){
    cachesCriadas=cc;
    cachesEncontradas=ce;
    dPercorrida=dp;
    pontosTotais=pt;
    }
    
     public Estatistica(Estatistica e){
    this.cachesCriadas=e.getCachesCriadas();
    this.cachesEncontradas=e.getCachesEncontradas();
    this.dPercorrida=e.getDPercorrida();
    this.pontosTotais=e.getPontosTotais();
    }
    
    public int getCachesCriadas(){return this.cachesCriadas;}
    public int getCachesEncontradas(){return this.cachesEncontradas;}
    public int getDPercorrida(){return this.dPercorrida;}
    public int getPontosTotais(){return this.pontosTotais;}
    
    public void setCachesCriadas(int cc){this.cachesCriadas=cc;}
    public void setCachesEncontradas(int ce){this.cachesEncontradas=ce;}
    public void setDPercorrida(int dp){this.dPercorrida=dp;}
    public void setPontosTotais(int pt){this.pontosTotais=pt;}
    
    
    // metodos
    
    public void adicionaValores(int distancia,int pontos){
        this.cachesCriadas++;
        this.cachesEncontradas++;
        this.dPercorrida+=distancia;
        this.pontosTotais+=pontos;
    }
    
    @Override
    public Estatistica clone(){return new Estatistica(this);}
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder(); 
        s.append("Numero de caches criadas: ");
        s.append(cachesCriadas);
        s.append(System.lineSeparator());
        s.append("Numero de caches encontradas: ");
        s.append(cachesEncontradas);
        s.append(System.lineSeparator());
        s.append("Distancia total percorrida: ");
        s.append(dPercorrida);
        s.append(System.lineSeparator());
        s.append("Pontuašao: ");
        s.append(pontosTotais);
        return s.toString();
    }
    
    @Override
    public int hashCode(){
    return this.toString().hashCode();
    }
}
