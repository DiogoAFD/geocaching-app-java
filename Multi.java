import java.util.*;
import java.io.*;
import java.text.*;

public class Multi extends Cache implements Serializable{
    //variaveis ou constantes
    
    private ArrayList<Coordenadas> coords = new ArrayList<>();
    private ArrayList<String> objetos;
    private boolean geocoins=false;
   
    //construtores
    
    public Multi(double latitude,double longitude,String info,String dono,int id,ArrayList<Coordenadas> coords,ArrayList<String> obj){
       super(latitude,longitude,info,dono,id);
       this.coords=coords;
       this.objetos=obj;
       
    }
    
    public Multi(Multi m){
        super(m);
        this.coords=m.getCoords();
        this.objetos=m.getObjetos();
    }
    
    //getters
    
     public ArrayList<Coordenadas> getCoords(){
        ArrayList<Coordenadas> aux= new ArrayList<Coordenadas>();
        
        for(Coordenadas a : this.coords)
            aux.add(a.clone());
        return aux;
    }
    
     public ArrayList<String> getObjetos(){
        ArrayList<String> aux=new ArrayList<>();
        for(String objeto:this.objetos){ 
            aux.add(objeto);
        }
        return aux;
    }
    //setters
    
    public void setGeocoins(boolean geocoins){this.geocoins=geocoins;}
    
    // metodos
    
    @Override
    public Multi clone(){
        return new Multi(this);
    
    }
    
    @Override
    
    public boolean equals(Object o){

        return (o==null || o.getClass()!=this.getClass()) ? false : this.getLatLong().equals(((Multi)o).getLatLong()) && this.coords.equals(((Multi)o).getCoords());
    }
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Cache-Multi");
        s.append(System.lineSeparator());
        s.append("Latitude inicial: ");
        s.append(getLatitude());
        s.append(System.lineSeparator());
        s.append("Longitude inicial: ");
        s.append(getLongitude());
        s.append(System.lineSeparator());
        s.append("Informação: ");
        s.append(getInfo());
        s.append(System.lineSeparator());
        s.append("ID: ");
        s.append(getID());
        s.append(System.lineSeparator());
        return s.toString();
    } 
    
    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
      
}