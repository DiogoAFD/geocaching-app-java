import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.io.*;

public class BaseDados implements Serializable{
    private HashMap<String,ArrayList<Cache>> baseUsuario;  //chaves ordenadas por usuario que as criou
    private HashMap<String,ArrayList<Cache>> basePais;  //chaves ordenadas por pais de origem do usuario que as criou
    private HashMap<String,ArrayList<Cache>> baseRegiao;    //chaves ordenadas por regiao ...
    //private HashMap<String,HashMap<String,ArrayList<Cache>>> basePais;     
    //private HashMap<String,HashMap<String,ArrayList<Cache>>> baseRegiao;   

    public BaseDados(){
        baseUsuario=new HashMap<String,ArrayList<Cache>>();
        basePais=new HashMap<String,ArrayList<Cache>>();
        baseRegiao=new HashMap<String,ArrayList<Cache>>();
        //basePais=new HashMap<String,HashMap<String,ArrayList<Cache>>>();
        // baseRegiao=new HashMap<String,HashMap<String,ArrayList<Cache>>>();
    }

    public BaseDados(BaseDados bd){
        this.baseUsuario=bd.getBaseUsuario();
        this.basePais=bd.getBasePais();
        this.baseRegiao=bd.getBaseRegiao();
    }

    //gets

    public HashMap<String,ArrayList<Cache>> getBaseUsuario(){
        HashMap<String,ArrayList<Cache>> aux= new HashMap<String,ArrayList<Cache>>();
        for(Map.Entry<String,ArrayList<Cache>> entry : baseUsuario.entrySet()) {
            String email = entry.getKey();
            ArrayList<Cache> listacache = entry.getValue();
            aux.put(email,clonaListaCache(listacache));
        }
        return aux;
    }

    public HashMap<String,ArrayList<Cache>> getBasePais(){
        HashMap<String,ArrayList<Cache>> aux= new HashMap<String,ArrayList<Cache>>();
        for(Map.Entry<String,ArrayList<Cache>> entry : basePais.entrySet()) {
            String pais = entry.getKey();
            ArrayList<Cache> listacache = entry.getValue();
            aux.put(pais,clonaListaCache(listacache));
        }
        return aux;
    }

    public HashMap<String,ArrayList<Cache>> getBaseRegiao(){
        HashMap<String,ArrayList<Cache>> aux= new HashMap<String,ArrayList<Cache>>();
        for(Map.Entry<String,ArrayList<Cache>> entry : baseRegiao.entrySet()) {
            String regiao = entry.getKey();
            ArrayList<Cache> listacache = entry.getValue();
            aux.put(regiao,clonaListaCache(listacache));
        }
        return aux;
    }

    /*
    public HashMap<String,HashMap<String,ArrayList<Cache>>> getBasePais(){
    HashMap<String,HashMap<String,ArrayList<Cache>>> aux= new HashMap<>();
    HashMap<String,ArrayList<Cache>> aux2=new HashMap<>();
    for(Map.Entry<String,HashMap<String,ArrayList<Cache>>> entry : basePais.entrySet()) {
    String pais = entry.getKey();
    HashMap<String,ArrayList<Cache>> hashcache = entry.getValue();
    for(Map.Entry<String,ArrayList<Cache>> entry2:hashcache.entrySet()){
    String email = entry2.getKey();
    ArrayList<Cache> listacacheaux = entry2.getValue();
    aux2.put(email,clonaListaCache(listacacheaux));
    }
    aux.put(pais,aux2);
    }
    return aux;
    }

    public HashMap<String,HashMap<String,ArrayList<Cache>>> getBaseRegiao(){
    HashMap<String,HashMap<String,ArrayList<Cache>>> aux= new HashMap<>();
    HashMap<String,ArrayList<Cache>> aux2=new HashMap<>();
    for(Map.Entry<String,HashMap<String,ArrayList<Cache>>> entry : basePais.entrySet()) {
    String regiao = entry.getKey();
    HashMap<String,ArrayList<Cache>> hashcache = entry.getValue();
    for(Map.Entry<String,ArrayList<Cache>> entry2:hashcache.entrySet()){
    String email = entry2.getKey();
    ArrayList<Cache> listacacheaux = entry2.getValue();
    aux2.put(email,clonaListaCache(listacacheaux));
    }
    aux.put(regiao,aux2);
    }
    return aux;
    }
     */

    //sets

    public void setBaseUsuario(){

    }

    public void setBasePais(){

    }

    public void setBaseRegiao(){

    }

    //metodos
    public void addBaseDados(Usuario u){
        ArrayList<Cache> aux;
        this.baseUsuario.put(u.getEmail(),u.getListaCaches());
        if(basePais.containsKey(u.getPais())){
            aux=basePais.get(u.getPais());
            for(Cache c:u.getListaCaches()){
                if(!aux.contains(c)) aux.add(c);
            }
            this.basePais.put(u.getPais(),aux);
        }
        else this.basePais.put(u.getPais(),u.getListaCaches());
        
        if(baseRegiao.containsKey(u.getRegiao())){
            aux=baseRegiao.get(u.getRegiao());
            for(Cache c:u.getListaCaches()){
                if(!aux.contains(c)) aux.add(c);
            }
            this.baseRegiao.put(u.getRegiao(),aux);
        }
        else this.baseRegiao.put(u.getRegiao(),u.getListaCaches());
    }

    public void removeBaseDados(Usuario u){
        this.baseUsuario.remove(u.getEmail(),u.getListaCaches());
        this.basePais.remove(u.getPais(),u.getListaCaches());
        this.baseRegiao.remove(u.getRegiao(),u.getListaCaches());
    }

    public void remCacheBD(Usuario u,Cache c){
        this.baseUsuario.get(u.getEmail()).remove(c);
        this.basePais.get(u.getPais()).remove(c);
        this.baseRegiao.get(u.getRegiao()).remove(c);
    }
    
    //metodos auxiliares
    public ArrayList<Cache> clonaListaCache(ArrayList<Cache> lc){
        ArrayList<Cache> lca=new ArrayList<>();
        for(Cache a:lc){
            lca.add(a.clone());
        }
        return lca;
    }

    public int removeCacheBD(Usuario u,int x){
        baseUsuario.get(u.getEmail()).remove(x);
        return x-1;
    }

    public void gravaObj(String fich) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fich));
        oos.writeObject(this);
        oos.flush(); oos.close();
    }

}
