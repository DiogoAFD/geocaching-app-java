import java.io.*;
import java.util.*;
import java.text.*;

public class Usuario implements Serializable{
    private String email;
    private String password;
    private String nome; 
    private String genero; //1- MASCULINO 2-FEMENINO
    private String pais;
    private String regiao;
    private String cidade;
    private GregorianCalendar dataNascimento; // dd/mm/aaaa
    private ArrayList<Cache> listaCaches;
    private TreeSet<Usuario> listaAmigos;
    private ArrayList<Usuario> listaPedidos;
    private HashMap<Integer,ArrayList<Estatistica>> estatisticas; //<ano,lista com estatisticas de cada mes> cada pos do array representa um mes
    private TreeSet<Actividade> listaActividades;
    private Coordenadas ultimaVisita;
    private Calendar horaUltimaVisita;
    
    
    //construtor parametrizado
    public Usuario(String email,String password,String nome,String genero,String pais,String regiao,String cidade,GregorianCalendar dataNascimento){
        this.email=email;
        this.password=password;
        this.nome=nome;
        this.genero=genero;
        this.pais=pais;
        this.regiao=regiao;
        this.cidade=cidade;
        this.dataNascimento=dataNascimento;
        this.listaCaches=new ArrayList<Cache>();
        this.listaAmigos=new TreeSet<Usuario>(new UsuarioComparator());
        this.listaPedidos=new ArrayList<Usuario>();
        this.estatisticas=new HashMap<Integer,ArrayList<Estatistica>>();
        this.listaActividades= new TreeSet<Actividade>(new ActividadeComparator());  
        horaUltimaVisita=Calendar.getInstance(); //receber hora actual instanciando
        horaUltimaVisita.setTimeInMillis(0); // hora passa a "default" 0
        ultimaVisita=new Coordenadas(); // posi�ao inicial 0,0
        
    }
    //construtor de copia 
    public Usuario(Usuario u){
        this.email=u.getEmail();
        this.password=u.getPassword();
        this.nome=u.getNome();
        this.genero=u.getGenero();
        this.pais=u.getPais();
        this.regiao=u.getRegiao();
        this.cidade=u.getCidade();
        this.dataNascimento=u.getDataNascimento();
        this.listaCaches=u.getListaCaches();
        this.listaAmigos=u.getListaAmigos();
        this.listaPedidos=u.getListaPedidos();
        this.estatisticas=u.getEstatisticas();
        this.listaActividades=u.getListaActividades();
        this.horaUltimaVisita=u.getHoraUltimaVisita();
        this.ultimaVisita=u.getUltimaVisita();
    }
    
    //Gets 
    
    public String getEmail(){return this.email;}
    public String getPassword(){ return this.password;}
    public String getNome(){return this.nome;}
    public String getGenero(){return this.genero;}
    public String getPais(){return this.pais;}
    public String getRegiao(){return this.regiao;}
    public String getCidade(){return this.cidade;}
    //stack overflow
    public GregorianCalendar getDataNascimento(){ return (GregorianCalendar) this.dataNascimento.clone();}
    public ArrayList<Cache> getListaCaches(){
        ArrayList<Cache> aux= new ArrayList<Cache>();
        
        for(Cache a : this.listaCaches)
            aux.add(a.clone());
       return aux;
    }
    public TreeSet<Usuario> getListaAmigos(){
       TreeSet<Usuario> lista_amigos=new TreeSet<Usuario>(new UsuarioComparator());
       Iterator<Usuario> it=this.listaAmigos.iterator();
       while(it.hasNext()){lista_amigos.add(it.next());}
       return lista_amigos;
    }
    
    public TreeSet<Actividade> getListaActividades(){
       TreeSet<Actividade> lista_actividades=new TreeSet<Actividade>(new ActividadeComparator());
       Iterator<Actividade> it=this.listaActividades.iterator();
       while(it.hasNext()){lista_actividades.add(it.next());}
       return lista_actividades;
    }
    
    public ArrayList<Usuario> getListaPedidos(){
        ArrayList<Usuario> aux= new ArrayList<Usuario>();
        
        for(Usuario a : this.listaPedidos)
            aux.add(a.clone());
        return aux;
    }
    
    public HashMap<Integer,ArrayList<Estatistica>> getEstatisticas(){
    HashMap<Integer,ArrayList<Estatistica>> aux=new HashMap<Integer,ArrayList<Estatistica>>();
       for(Map.Entry<Integer,ArrayList<Estatistica>> entry : this.estatisticas.entrySet()) {
            Integer ano = entry.getKey();
            ArrayList<Estatistica> listaestatistica = entry.getValue();
            aux.put(ano,clonaListaEstatistica(listaestatistica));
       }
    return aux;
    }
    
    public Calendar getHoraUltimaVisita(){ return (Calendar) this.horaUltimaVisita.clone();}
    
    public Coordenadas getUltimaVisita(){return this.ultimaVisita.clone();}
    
    // Setters
    
    public void setEmail(String email){this.email=email;}
    
    public void setPassword(String password){ this.password=password;}
    
    public void setNome(String nome){this.nome=nome;}
    
    public void setGenero(String genero){this.genero=genero;}
    
    public void setPais(String pais){this.pais=pais;}
    
    public void setRegiao(String regiao){this.regiao=regiao;}
    
    public void setCidade(String cidade){this.cidade=cidade;}
    
    public void setDataNascimento(GregorianCalendar dataNascimento){ this.dataNascimento=dataNascimento;}
    
    public void setListaCaches(ArrayList<Cache> ar){
        
        ArrayList<Cache> aux = new ArrayList<Cache>();
        for(Cache c:ar)
            aux.add(c.clone());
        this.listaCaches=ar;
    }
    
    public void setListaAmigos(TreeSet <Usuario> u){
       TreeSet<Usuario> lista_amigos=new TreeSet<Usuario>(new UsuarioComparator());
       Iterator<Usuario> it=u.iterator();
       while(it.hasNext()){
            lista_amigos.add(it.next());
       }
       this.listaAmigos=lista_amigos;
    }
      public void setListaActividades(TreeSet <Actividade> la){
       TreeSet<Actividade> lista_actividades=new TreeSet<Actividade>(new ActividadeComparator());
       Iterator<Actividade> it=la.iterator();
       while(it.hasNext()){
            lista_actividades.add(it.next());
       }
       this.listaActividades=lista_actividades;
    }
    
    public void setListaPedidos(ArrayList<Usuario> x){
        ArrayList<Usuario> aux=new ArrayList<Usuario>();
        for(Usuario u:x)
            aux.add(u.clone());
        this.listaPedidos=aux;
    }
    
    public void setUltimaVisita(Coordenadas ultimaVisita){this.ultimaVisita=ultimaVisita;}
    public void setHoraUltimaVisita(Calendar horaUltimaVisita){this.horaUltimaVisita=horaUltimaVisita;}
    
    public void setEstatisticas(HashMap<Integer,ArrayList<Estatistica>> hashest){
    this.estatisticas=hashest;
    }
    
    
    //metodo auxiliar para clonar arraylist
    
    public ArrayList<Estatistica> clonaListaEstatistica(ArrayList<Estatistica> le){
       ArrayList<Estatistica> lea=new ArrayList<Estatistica>();
       for(Estatistica e:le){
           lea.add(e.clone());
        }
        return lea;
       
    }
    
    /**
     * equals,clone,toString
     */
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;
        }
        Usuario a=(Usuario) o;
        return (this.getEmail().equals(a.getEmail()));
    }
    
    public Usuario clone(){
        return new Usuario(this);
    }
    
    public String toString(){  
        StringBuilder s = new StringBuilder(); 
        s.append("Email: ");
        s.append(email);
        s.append(System.lineSeparator());
        s.append("Nome: ");
        s.append(nome);
        s.append(System.lineSeparator());
        s.append("Genero ");
        s.append(genero);
        s.append(System.lineSeparator());
        s.append("Pais ");
        s.append(pais);
        s.append(System.lineSeparator());
        s.append("Regiao ");
        s.append(regiao);
        s.append(System.lineSeparator());
        s.append("Cidade ");
        s.append(cidade);
        s.append(System.lineSeparator());
        s.append("Data de nascimento: ");
        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(dataNascimento.getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
    }
    
    public void addPedido(Usuario u){
        this.listaPedidos.add(u);
    }
    
    public void removeAmigo(String email){
        this.listaAmigos.remove(email);
    }
    //usar size e refazer
    public int removePedido (int x){
        this.listaPedidos.remove(x);
        return x-1;
    }
    
    public void addAmigo(Usuario a){
        this.listaAmigos.add(a.clone());
    }
    
 
    public void remAmigo(Usuario a){
        this.listaAmigos.remove(a);
    }
    
    public void addCache(Cache c)throws CacheExisteException{
        if(this.listaCaches.contains(c))
            throw new CacheExisteException();
        this.listaCaches.add(c);
    }
    
    public void addActividade(Actividade a){
        this.listaActividades.add(a.clone());
    }
    
    public void remCache(Cache c){
        this.listaCaches.remove(c);
    }
}