import java.util.*;
import java.io.*;
import java.text.*;

public class Misterio extends Cache implements Serializable{
    //variaveis ou constantes
    
    private String pergunta;
    private String resposta;
    private double latitudeF;
    private double longitudeF;
    private Coordenadas latlongF;
    private ArrayList<String> objetos;
    private boolean geocoins=false;
    
    //contrutores
    public Misterio(double latitudeI, double longitudeI,double latitudeF,double longitudeF, String info,String pergunta,String resposta,String dono,int id,ArrayList<String> obj){
        super(latitudeI,longitudeI,info,dono,id);
        this.latitudeF=latitudeF;
        this.longitudeF=longitudeF;
        latlongF=new Coordenadas(latitudeF,longitudeF);
        this.pergunta=pergunta;
        this.resposta=resposta;
        this.objetos=obj;
    }
    
    public Misterio(Misterio m){
        super(m);
        this.latitudeF=m.getLatitudeF();
        this.longitudeF=m.getLongitudeF();
        this.latlongF=m.getLatLongF();
        this.pergunta=m.getPergunta();
        this.resposta=m.getResposta();
        this.objetos=m.getObjetos();
    }
    //getters
    
    public double getLatitudeF(){return this.latitudeF;}
    
    public double getLongitudeF(){return this.longitudeF;}
     
    public Coordenadas getLatLongF(){return this.latlongF;}
     
    public String getPergunta(){return this.pergunta;}
    
    public String getResposta(){return this.resposta;}
    
     public ArrayList<String> getObjetos(){
        ArrayList<String> aux=new ArrayList<>();
        for(String objeto:this.objetos){ 
            aux.add(objeto);
        }
        return aux;
    }
    
    //setters
    
    public void setPergunta(String pergunta){this.pergunta=pergunta;}
    
    public void setResposta(String resposta){this.resposta=resposta;}
    
    public void setLatitudeF(double latitudeF){this.latitudeF=latitudeF;}
    
    public void setLongitudeF(double longitudeF){this.longitudeF=longitudeF;}
    
    public void setLatLongF(Coordenadas latlongF){this.latlongF=latlongF;}
    
    //metodos 
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("Cache-Mist�rio");
        s.append(System.lineSeparator());
        s.append("Latitude: ");
        s.append(getLatitude());
        s.append(System.lineSeparator());
        s.append("Longitude: ");
        s.append(getLongitude());
        s.append(System.lineSeparator());
        s.append("Informa��o: ");
        s.append(getInfo());
        s.append(System.lineSeparator());
        s.append("Pergunta: ");
        s.append(getPergunta());
        s.append(System.lineSeparator());
        return s.toString();
    } 

    @Override
    public Misterio clone(){
        return new Misterio(this);
    
    }
    
    @Override
    public boolean equals(Object o){
        //Misterio m=(Misterio) o; nao fazer cast se as classes nao forem iguais , possivel erro
        return (o==null || o.getClass()!=this.getClass()) ? false : this.latlongF.equals(((Misterio)o).getLatLongF()) ; 
    }
    
    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
}