import java.util.*;
import java.io.*;


public class UsuarioComparator implements Comparator<Usuario>,Serializable{
    public int compare(Usuario u1 ,Usuario u2){
        if(u1.equals(u2)) return 0;
        if(u1.getEmail().compareTo(u2.getEmail())!= 0) return u1.getEmail().compareTo(u2.getEmail());
        return 1;
    }
}