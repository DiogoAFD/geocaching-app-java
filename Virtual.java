import java.util.*;
import java.io.*;
import java.text.*;

public class Virtual extends Cache implements Serializable{

    //construtores
    
    public Virtual(double latitude, double longitude, String info,String dono,int id){
        super(latitude,longitude,info,dono,id);
    }

    public Virtual(Virtual v){
        super(v);
    }

    // metodos

    @Override
    public Virtual clone(){
        return new Virtual(this);
    }

    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    @Override
    public boolean equals(Object o){
        return (o==null || o.getClass()!=this.getClass()) ? false : this.getLatLong().equals(((Virtual)o).getLatLong());
    }


}