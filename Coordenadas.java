import java.io.*;
import java.util.*;
import java.text.*;

public class Coordenadas implements Serializable{
    private double latitude;
    private double longitude;
    public Coordenadas(){
        this.latitude = 0.0;
        this.longitude = 0.0;
    }
    public Coordenadas(double latitude,double longitude){
        this.latitude=latitude;
        this.longitude=longitude;
    }
    public Coordenadas(Coordenadas c){
        this.latitude=c.getLatitude();
        this.longitude=c.getLongitude();
    }
    
    public double getLatitude(){return this.latitude;}
    public double getLongitude(){return this.longitude;}
    public void setLatitude(double latitude){this.latitude=latitude;}
    public void setLongitude(double longitude){this.longitude=longitude;}
    public void setCoordenadas(double latitude,double longitude){this.latitude=latitude;this.longitude=longitude;}
    
    public boolean equals (Object o){
        Coordenadas c =(Coordenadas) o;
        return (c==null || c.getClass()!=this.getClass()) ? false :this.latitude==c.getLatitude() && this.longitude==c.getLongitude() ;
    }
    
    public Coordenadas clone(){
        return new Coordenadas (this); 
    }
    public String toString(){ 
     StringBuilder s = new StringBuilder(); 
     s.append("Latitude: ");
     s.append(getLatitude());
     s.append(System.lineSeparator());
     s.append("Longitude: ");
     s.append(getLongitude());
     return s.toString();
    }
    
    public int hashCode(){
        return this.toString().hashCode();
    }
}