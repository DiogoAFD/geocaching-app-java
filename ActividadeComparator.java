import java.util.*;
import java.io.*;


public class ActividadeComparator implements Comparator<Actividade>,Serializable{
   @Override
   public int compare(Actividade a1 ,Actividade a2){
       if(a1.equals(a2)) return 0;
       if(a1.getDataReg().compareTo(a2.getDataReg())!= 0) return a1.getDataReg().compareTo(a2.getDataReg());
       return 1;
   }
}