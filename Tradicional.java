import java.util.ArrayList;
public class Tradicional extends Cache{
    
    //variaveis ou contantes
    private ArrayList<String> objetos;
    private boolean geocoins=false;
    
    
    //construtores
    public Tradicional(double latitude,double longitude,String info,ArrayList<String> objetos,String dono,int id){
       super(latitude,longitude,info,dono,id);
       this.objetos=objetos;
    }
    
    public Tradicional(Tradicional t){
        super(t);
        this.geocoins=t.getGeocoins();
        this.objetos=t.getObjetos();
    }
    //gettes
    
    public boolean getGeocoins(){return this.geocoins;}
    
    public ArrayList<String> getObjetos(){
        ArrayList<String> aux=new ArrayList<>();
        for(String objeto:this.objetos){ 
            aux.add(objeto);
        }
        return aux;
    }
    
    //setters
    
    public void setGeocoins(boolean geocoins){this.geocoins=geocoins;}
    
    // metodos
    
    @Override
    public boolean equals(Object o){
        return (o==null || o.getClass()!=this.getClass()) ? false : this.getLatLong().equals(((Tradicional)o).getLatLong());
    }
    
    @Override
    public Tradicional clone(){
    return new Tradicional(this);
    }
    
    @Override
    public int hashCode(){
    return this.toString().hashCode();
    }
}
