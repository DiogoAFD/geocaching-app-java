import java.util.*;
import java.io.*;

public class Rede implements Serializable{
    
    TreeMap<String,Usuario> rede;
    
    public Rede(){
        rede=new TreeMap<String,Usuario>();
        
    }
    
    public Rede(Rede r){
        rede=r.getRede();
    }

    public TreeMap<String,Usuario> getRede(){
        TreeMap<String,Usuario> aux=new TreeMap<String,Usuario>();
        for(Usuario u:rede.values())
            aux.put(u.getEmail(),u.clone());
        return aux;
    }
    
    public void setRede(TreeMap<String,Usuario> r){
        TreeMap<String,Usuario> aux=new TreeMap<String,Usuario>();
        for(Usuario u:r.values())
            aux.put(u.getEmail(),u.clone());
        rede=aux;
    }
    
    public void addUsuario(Usuario u)throws UsuarioExisteException{
        if(rede.containsKey(u.getEmail()))
            throw new UsuarioExisteException(u.getEmail());
        rede.put(u.getEmail(),u.clone());
    }
    
    public void removeUsuario(Usuario u) throws UsuarioExisteException{
        if(!rede.containsKey(u.getEmail()))
            throw new UsuarioExisteException(u.getEmail());
        rede.remove(u.getEmail());
    }
    
    public int login(String email,String password) throws UsuarioExisteException{
        if(!rede.containsKey(email))
            throw new UsuarioExisteException(email);
        if(rede.get(email).getPassword().equals(password))
            return 1;
        else return 0;
    }
    
    public void gravaObj(String fich) throws IOException {
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fich));
      oos.writeObject(this);
      oos.flush(); oos.close();
    } 
    
    public Usuario getUser(String u){
        return rede.get(u).clone();
    }
    
    public void enviaPedido(String u,String a) throws UsuarioExisteException, AmigoExisteException{
        if(!rede.containsKey(a))
            throw (new UsuarioExisteException());
        if(rede.get(u).getListaAmigos().contains(rede.get(a)))
            throw (new AmigoExisteException());
        
        rede.get(a).addPedido(rede.get(u));
        
    }

    public void replaceUsuario (Usuario u){
        rede.put(u.getEmail(),u.clone());
    }
    
    public Rede clone(){
        return new Rede (this);
    }
    
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;
        }
        Rede a=(Rede) o;
        int cont=0;
        for(Usuario u:rede.values()){
            if(u.equals(a.getUser(u.getEmail())))
                cont++;
        }
        if(cont==this.rede.size())
            return true;
        return false;
        
    }
    
    
     public String toString(){  
        StringBuilder s = new StringBuilder(); 
        for(Usuario u:this.rede.values()){
            s.append(u.toString());
        }
       s.append(System.lineSeparator());
       return s.toString();
    }
}
