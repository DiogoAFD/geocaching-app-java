import java.util.*;
import java.io.*;

public class CacheExisteException extends Exception implements Serializable{
    public CacheExisteException(){
        super();
    }
    
    public CacheExisteException(String s){
        super(s);
    }
}
