import java.util.*;
import java.io.*;
import java.text.*;

public class Micro extends Cache implements Serializable{
    
    //construtores
    
    public Micro(double latitude, double longitude, String info,String dono,int id){
        super(latitude,longitude,info,dono,id);
    }
    
    public Micro(Micro m){
        super(m);
    }
    
    // metodos
    @Override
    public Micro clone(){
        return new Micro(this);
    
    }
  
    
    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    @Override
    public boolean equals(Object o){
        return (o==null || o.getClass()!=this.getClass()) ? false : this.getLatLong().equals(((Micro)o).getLatLong());
    }
      
}

