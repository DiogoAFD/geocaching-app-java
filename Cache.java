import java.lang.*;
import java.util.*;
import java.io.*;

public class Cache implements Serializable{

    //variaveis ou constantes
    private double latitude;
    private double longitude;
    private String info;
    private String dono;
    private ArrayList<String> livroReg;
    private int id;
    protected Coordenadas latlong;
    
    //Construtores    nota : vazio n faz sentido
    public Cache(double latitude,double longitude,String info,String dono,int id){
        this.latitude=latitude;
        this.longitude=longitude;
        this.info=info;
        livroReg=new ArrayList<String>();
        this.dono=dono;
        this.id=id;
        latlong=new Coordenadas(latitude,longitude);
    }
    
    public Cache(Cache c){
        this.latitude=c.getLatitude();
        this.longitude=c.getLongitude();
        this.info=c.getInfo();
        this.livroReg=c.getLivroReg();
        this.dono=c.getDono();
        id=c.getID();
        this.latlong=c.getLatLong();
        
    }
    
    //getters
    
    public double getLatitude(){return this.latitude;}
    
    public double getLongitude(){return this.longitude;}
    
    public int getID(){return this.id;}
    
    public String getDono(){return this.dono;}
    
    public String getInfo(){return this.info;}
    
    public ArrayList<String> getLivroReg(){
        ArrayList<String> aux=new ArrayList<>();
        for(String email:this.livroReg){ 
            aux.add(email);
        }
    
        return aux;
    }
    
    public Coordenadas getLatLong(){return this.latlong;}
   
    //setters
    
    public void setLatitude(double latitude){this.latitude=latitude;}
    
    public void setLongitude(double longitude){this.longitude=longitude;}
    
    public void setID(int id){this.id=id;}
    
    public void setInfo(String info){this.info=info;}
    
    public void setDono(String dono){this.dono=dono;}
    
    public void setLatLong(Coordenadas latlong){this.latlong=latlong;}
    
    public void setLivroReg(ArrayList<String> lr){
        ArrayList<String> aux = new ArrayList<>();
        for(String s:lr){
            aux.add(s);
        }
        this.livroReg=lr;
    }

    //metodos
    
    public void addLivroReg(String r){
       this.livroReg.add(r);
    }
    
    
    @Override
    public Cache clone(){
        return new Cache(this);
    
    }
    
    
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(this.getClass().getName());
        s.append(System.lineSeparator());
        s.append("Latitude: ");
        s.append(this.latitude);
        s.append(System.lineSeparator());
        s.append("Longitude: ");
        s.append(this.longitude);
        s.append(System.lineSeparator());
        s.append("Informação: ");
        s.append(this.info);
        s.append(System.lineSeparator());
        s.append("Dono da cache: ");
        s.append(this.dono);
        s.append(System.lineSeparator());
        s.append("ID: ");
        s.append(this.id);
        return s.toString();
    }

    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
}