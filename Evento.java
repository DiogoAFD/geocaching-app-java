import java.util.*;
import java.io.*;
import java.text.*;

public class Evento extends Cache implements Serializable{

    // construtores
    
    public Evento(double latitude, double longitude, String info,String dono,int id){
        super(latitude,longitude,info,dono,id);
    }
    
    public Evento(Evento e){
        super(e);
    }
    
    // metodos

    @Override
    public Evento clone(){
        return new Evento(this);
    }
    
    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    @Override
    public boolean equals(Object o){
        return (o==null || o.getClass()!=this.getClass()) ? false : this.getLatLong().equals(((Evento)o).getLatLong());
    }
      
}
