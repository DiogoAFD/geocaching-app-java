import java.util.*;
import java.io.*;

public class UsuarioExisteException extends Exception implements Serializable{
    public UsuarioExisteException(){
        super();
    }
    
    public UsuarioExisteException(String s){
        super(s);
    }
}