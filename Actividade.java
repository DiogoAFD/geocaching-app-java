import java.util.*;
import java.io.*;
import java.text.*;
import java.util.Calendar;

public class Actividade implements Serializable{
   
    //variaveis ou constantes
    private String donoCache;
    private GregorianCalendar dataReg;
    private String tipoCache;
    //private double longitudeReg; deprecated
    //private double latitudeReg;
    private Coordenadas latlong;
    private Meteorologia condMetReg;
    private ArrayList<String> objectosRecolhidos;
    private int distancia; //distancia em KM percorrida ate encontrar a cache
    private int pontos;
    
    //construtores
    
    public Actividade(String donoC,String tipoC,Coordenadas latlong,Meteorologia condMet,ArrayList<String> objRec,int distancia){
        this.donoCache=donoC;
        this.tipoCache=tipoC;
        this.latlong=latlong;
        this.condMetReg=condMet;
        this.objectosRecolhidos=objRec;
        dataReg=new GregorianCalendar();
        this.distancia=distancia;
    }
    
    public Actividade(Actividade a){
        this.donoCache=a.getDonoCache();
        this.dataReg=a.getDataReg();
        this.tipoCache=a.getTipoCache();
        this.latlong=a.getLatLong();
        this.condMetReg=a.getCondMet();
        this.objectosRecolhidos=a.getObjectosRecolhidos();
        this.distancia=a.getDistancia();
    }
    
    
    //getters
    
    public String getDonoCache(){return this.donoCache;}
    
    public GregorianCalendar getDataReg(){return (GregorianCalendar) this.dataReg.clone();}
    
    public String getTipoCache(){return this.tipoCache;}
    
    public Coordenadas getLatLong(){return this.latlong.clone();}
    
    public Meteorologia getCondMet(){return this.condMetReg.clone();}
    
    public ArrayList<String> getObjectosRecolhidos(){
        ArrayList<String> aux=new ArrayList<>();
        if(this.objectosRecolhidos.isEmpty()) aux.add("nenhum Obj");
        for(String o:this.objectosRecolhidos){
            aux.add(o);

        }
        return aux;
    }
    
    public int getDistancia(){return this.distancia; }
    
    public int getPontos(){return this.pontos;}
    
    //setters
    
    public void setDonoCache(String dono){this.donoCache=dono;}
    
    public void setDataReg(GregorianCalendar data){this.dataReg=data;}
    
    public void setTipoCache(String tipo){this.tipoCache=tipo;}
    
    public void setLatLong(Coordenadas latlong){this.latlong=latlong;}
    
    public void setCondMet(Meteorologia cm){this.condMetReg=cm;}
    
    public void setObjectosRecolhidos(ArrayList<String> or){
        for(String o:or){
            objectosRecolhidos.add(o);
        }
    }
    
    public void setDistancia(int distancia){this.distancia=distancia;}
    
    public void setPontos(int pontos){this.pontos=pontos;}
    
    
    //metodos
    
    //Calcula pontua�ao de acordo com as informa�oes da actividade 
    public int calcPontosA(){
        int pontos=0;
        if(distancia < 10) pontos=pontos+1;
        if(distancia < 30) pontos=pontos+2;
        if(distancia < 50) pontos=pontos+3;
        if(distancia < 70) pontos=pontos+4;
        if(distancia < 100) pontos=pontos+5;
        pontos=pontos+this.condMetReg.calcPontos();
        switch(this.tipoCache){
        
            case "Misterio"     : pontos=pontos+5;
                break;
            case "Multi"        : pontos=pontos+4;
                break;
            case "Tradicional"  : pontos=pontos+3;
                break;
            case "Micro"        : pontos=pontos+3;
                break;
            case "Virtual"      : pontos=pontos+2;
                break;
            case "Evento"       : pontos=pontos+1;
        }
    
        return pontos;
    }
    
    @Override
    public Actividade clone(){
        return new Actividade(this);
    }
    
    @Override
    public boolean equals(Object o){
        Actividade a=(Actividade) o;
        return (a==null || a.getClass()!=this.getClass()) ? false : this.latlong==a.getLatLong() && this.donoCache==a.getDonoCache() && this.tipoCache==a.getTipoCache();
    }
    
    @Override
    public String toString(){
        StringBuilder s=new StringBuilder();
        s.append("Dono da cache : ");
        s.append(this.donoCache);
        s.append(System.lineSeparator());
        s.append("Data em que a cache foi registada : ");
        SimpleDateFormat dataRegAux= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
        String dataRegAuxS=dataRegAux.format(dataReg.getTime());
        s.append(dataRegAuxS);
        s.append(System.lineSeparator());
        s.append("Tipo de cache : ");
        s.append(this.tipoCache);
        s.append(System.lineSeparator());
        s.append("Coordenadas da cache : ");
        s.append(this.latlong);
        s.append(System.lineSeparator());
        s.append("Distancia percorrida : ");
        s.append(this.distancia);
        s.append(System.lineSeparator());
        s.append("Pontua�ao desta actvidades : ");
        s.append(this.pontos);
        s.append(System.lineSeparator());
        s.append("Condi�oes meteorologicas : ");
        s.append(System.lineSeparator());
        s.append(this.condMetReg);
        s.append(System.lineSeparator());
        s.append("Objectos recolhidos da cache : ");
        s.append(this.objectosRecolhidos);
        s.append(System.lineSeparator());
        return s.toString();
    }
}